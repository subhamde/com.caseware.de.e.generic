(function() {
  var /** @const */ custom = wpw.financials.custom;

  // Statement of Retained Earnings customizations
  var reAdditions = wpw.financials.StatementOfRetainedEarnings.customizations.additions;
  reAdditions.push(new custom.Addition(['Retained earnings, beginning of year', 'Deficit, beginning of year',
      'Retained earnings (deficit), beginning of year'], true, 'retained_earnings_beginning', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.INSERT, ['1.5.100.200.100'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, custom.TYPE.LINE));

  // Add net income
  reAdditions.push(new custom.Addition(['Net income', 'Net loss', 'Net income (loss)'], true, 'net_income',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['2'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1,
      custom.TYPE.LINE));

  // Add a total for Retained earnings and Net income
  reAdditions.push(new custom.Addition('', false, 'retained_earnings_subtotal', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.INSERT, ['1.5.100.200.100', 'netincome'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenTagZero: 'dividends', underline: 'none'}, 1, custom.TYPE.TOTAL));

  // Add a total for the two dividends
  reAdditions.push(new custom.Addition('Dividends', true, 'dividends', wpw.models.Tag.SIGN.DEBIT,
      custom.PLACEMENT.INSERT, ['1.5.100.200.600', '1.5.100.200.700', '1.5.100.200.800'], custom.ADD_SCHEDULE_LINE.TOP,
      {hideWhenTagZero: 'dividends'}, 1, custom.TYPE.LINE));

  // Add Ending retained earnings to the bottom of the Statement of Equity
  reAdditions.push(new custom.Addition(['Retained earnings, end of year', 'Deficit, end of year',
      'Retained earnings (deficit), end of year'], false, 'retained_earnings_end', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.INSERT, ['1.5.100.200'], custom.ADD_SCHEDULE_LINE.BOTTOM, {}, 1, custom.TYPE.TOTAL));
})();
