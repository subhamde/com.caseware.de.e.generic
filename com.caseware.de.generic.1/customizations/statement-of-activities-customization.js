(function() {
  var /** @const */ custom = wpw.financials.custom;

  /** @type {string} The name of the dimension category to print by */
  wpw.financials.StatementOfActivities.customizations.dimensionCategory = 'Restrictedness';

  var activitiesCustomization = wpw.financials.StatementOfActivities.customizations;

  activitiesCustomization.topLevelTags.push(new custom.TopLevelTagInfo('2'));
})();
