(function() {
  var /** @const */ custom = wpw.financials.custom;
  var is = wpw.financials.IncomeStatement.customizations;

  // Revenue
  is.topLevelTags.push(new custom.TopLevelTagInfo('2.4'));

  // Cost of sales
  is.topLevelTags.push(new custom.TopLevelTagInfo('2.5'));

  // Gross profit
  is.additions.push(new custom.Addition('Gross profit', false, 'gross_profit',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['2.4', '2.5'], custom.ADD_SCHEDULE_LINE.AFTER,
      {omitChildPadding: true, anchor: '2.5', underline: 'normal'}, 0, wpw.financials.custom.TYPE.TOTAL));

  // Operating expenses
  is.topLevelTags.push(new custom.TopLevelTagInfo('2.6', {extraPadding: 'top-small', underline: 'normal'}));

  // Income from operations
  is.additions.push(new custom.Addition(
      ['Income from operations', 'Loss from operations', 'Income (loss) from operations'], false,
      'income_from_operations', wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['gross_profit', '2.6'],
      custom.ADD_SCHEDULE_LINE.AFTER, {omitChildPadding: true, anchor: '2.6', underline: 'normal'}, 0,
      wpw.financials.custom.TYPE.TOTAL));

  // Non-operating income (expense)
  is.topLevelTags.push(new custom.TopLevelTagInfo('2.7', {extraPadding: 'top-small'}));

  // Income before income tax expense
  is.additions.push(new custom.Addition(
      ['Income before income tax expense', 'Loss before income tax expense', 'Income (loss) before income tax expense'],
      false, 'income_before_income_tax_expense', wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT,
      ['2.4', '2.5', '2.6', '2.7'], custom.ADD_SCHEDULE_LINE.AFTER,
      {omitChildPadding: true, anchor: '2.7', underline: 'normal'}, 0, wpw.financials.custom.TYPE.TOTAL));

  // Income tax expense (benefit)
  is.topLevelTags.push(new custom.TopLevelTagInfo('2.7.600'));

  // Other comprehensive income (loss)
  is.topLevelTags.push(new custom.TopLevelTagInfo('2.7.500'));

  // Net income
  is.additions.push(new custom.Addition(['Net income', 'Net loss', 'Net income (loss)'], false, 'inserted_net_income',
      wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.INSERT, ['2'], custom.ADD_SCHEDULE_LINE.BOTTOM, {}, 0,
      wpw.financials.custom.TYPE.TOTAL));
})();
