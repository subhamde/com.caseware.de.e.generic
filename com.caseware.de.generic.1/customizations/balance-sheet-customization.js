(function() {
  var /** @const */ custom = wpw.financials.custom;

  // Balance sheet customizations
  var bsAdditions = wpw.financials.BalanceSheet.customizations.additions;

  bsAdditions.push(new custom.Addition('Assets', true, 'assets', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.TOP_LEVEL,
      ['1.1', '1.2'], custom.ADD_SCHEDULE_LINE.TOP, {}, 0, wpw.financials.custom.TYPE.LINE));

  bsAdditions.push(new custom.Addition('Liabilities and equity', true, 'liabilities_and_equity', wpw.models.Tag.SIGN.CREDIT,
      custom.PLACEMENT.TOP_LEVEL, ['1.3', '1.4', '1.5'], custom.ADD_SCHEDULE_LINE.TOP,
      {extraPadding: 'top', omitChildPadding: true}, 0, wpw.financials.custom.TYPE.LINE));

  bsAdditions.push(new custom.Addition('Liabilities', true, 'liabilities', wpw.models.Tag.SIGN.CREDIT, custom.PLACEMENT.TOP_LEVEL,
      ['1.3', '1.4'], custom.ADD_SCHEDULE_LINE.NO, {}, 0, wpw.financials.custom.TYPE.LINE));
})();
