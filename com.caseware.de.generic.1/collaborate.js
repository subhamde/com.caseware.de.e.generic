// Declaring our DE Generic object
var DEImpl = function () {
  this.id = "77c98580-5c0d-4c87-a9c4-dd83be10e011";

  function notContact() {
    return !CWI.ProfileManager.get().isContact();
  }

  this.canOpen = notContact();
  this.canCreate = notContact();
  this.canDelete = notContact();

  this.openInNewTab = true;
  this.supportsQueries = true;

  this.versions = [{
	  "id":"com.caseware.de.generic.2",
	  "name":"Audit Year 2"
	  }, 
	  { "id":"com.caseware.de.generic.1", 
	  "name":"Audit Year 1"
	  }];
  this.versionsHeading = "Year";
  this.canRollForward = true;

  this.firmSettingsPages = [{
      "heading": "Audit Template 2017-18",
      "anchorId": "1",
      "url": "../e/template.jsp?t=" + this.id + "&s=com.caseware.de.generic.2"
    },
	{
      "heading": "Audit Template 2016-17",
      "anchorId": "2",
      "url": "../e/template.jsp?t=" + this.id + "&s=com.caseware.de.generic.1"
    }
	];

  this.gridIconSVG = "../e/prod/com.caseware.de.generic.1/icon.svg";
  this.doormatSVG = "../e/prod/com.caseware.de.generic.1/icon.svg";

  return this;
};

DEImpl.prototype = new CollaborateImpl();
collaborate.addModel(new DEImpl());
