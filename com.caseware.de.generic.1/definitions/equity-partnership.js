(function() {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var CORPORATION_CHECKLIST_ID = '0Xtk9-iaTAu_2qnq77uftA';
  var CORPORATION_PROCEDURE_ID = 'eZiFN4BBSQKMQlFaAY5-Bw';
  var CORPORATION_RESPONSE_ID = 'fzxYRRXSQnCV_48z-Oha3g';

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  var PARTNER_1_FORMULA = TAG('1.5.200.100');
  var PARTNER_2_FORMULA = TAG('1.5.200.200');
  var PARTNER_3_FORMULA = TAG('1.5.200.300');
  var PARTNER_4_FORMULA = TAG('1.5.200.400');
  var PARTNER_5_FORMULA = TAG('1.5.200.500');
  var CAPITAL_BEGIN_FORMULA = ADD(TAG('1.5.200.100.100'), TAG('1.5.200.200.100'), TAG('1.5.200.300.100'),
      TAG('1.5.200.400.100'), TAG('1.5.200.500.100'));
  var CONTRIBUTED_CAPITAL_FORMULA = ADD(TAG('1.5.200.100.105'), TAG('1.5.200.200.105'), TAG('1.5.200.300.105'),
      TAG('1.5.200.400.105'), TAG('1.5.200.500.105'));
  var DRAWS_FORMULA = ADD(TAG('1.5.200.100.110'), TAG('1.5.200.200.110'), TAG('1.5.200.300.110'),
      TAG('1.5.200.400.110'), TAG('1.5.200.500.110'));
  var INCOME_ALLOCATION_FORMULA = ADD(TAG('1.5.200.100.115'), TAG('1.5.200.200.115'), TAG('1.5.200.300.115'),
      TAG('1.5.200.400.115'), TAG('1.5.200.500.115'));
  /************ Formulas end *****/
  var equityDef = new wpw.analysis.AnalysisDefinition();
  equityDef.id = 'vgtTahp3RYq9ZaIhG5VEig';
  equityDef.name = 'Equity - Partnership';

  // Visible only when engagement is Corporate. Determined by answer from checklist.
  equityDef.visibilitySetting = new wpw.models.Visibility.Settings();
  equityDef.visibilitySetting.allConditionsNeeded = true;
  equityDef.visibilitySetting.normallyVisible = false;
  var isPartnersCond = new wpw.models.Visibility.ResponseCondition(CORPORATION_CHECKLIST_ID);
  isPartnersCond.procedureId = CORPORATION_PROCEDURE_ID;
  isPartnersCond.responseId = CORPORATION_RESPONSE_ID;
  equityDef.visibilitySetting.conditions.push(isPartnersCond);

  // Partners detail charts
  equityDef.rootTagsToTraverse[PARTNER_1_FORMULA.tagNumber] = PARTNER_1_FORMULA.tagNumber;
  equityDef.componentDefinitions.push(createDetailChart('SJMSaz30', 'Partner 1', PARTNER_1_FORMULA.tagNumber));
  equityDef.rootTagsToTraverse[PARTNER_2_FORMULA.tagNumber] = PARTNER_2_FORMULA.tagNumber;
  equityDef.componentDefinitions.push(createDetailChart('r1gzr6M3C', 'Partner 2', PARTNER_2_FORMULA.tagNumber));
  equityDef.rootTagsToTraverse[PARTNER_3_FORMULA.tagNumber] = PARTNER_3_FORMULA.tagNumber;
  equityDef.componentDefinitions.push(createDetailChart('SkbfBpG2C', 'Partner 3', PARTNER_3_FORMULA.tagNumber));
  equityDef.rootTagsToTraverse[PARTNER_4_FORMULA.tagNumber] = PARTNER_4_FORMULA.tagNumber;
  equityDef.componentDefinitions.push(createDetailChart('HkGMHTf30', 'Partner 4', PARTNER_4_FORMULA.tagNumber));
  equityDef.rootTagsToTraverse[PARTNER_5_FORMULA.tagNumber] = PARTNER_5_FORMULA.tagNumber;
  equityDef.componentDefinitions.push(createDetailChart('S1QGraz2R', 'Partner 5', PARTNER_5_FORMULA.tagNumber));

  function createDetailChart(id, name, rootKey) {
    var compDef = new ComponentDefinition(id, name, COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], rootKey, true, false);
    compDef.visibilityTagNumber = rootKey
    compDef.shouldDisplay = false;
    return compDef;
  }

  var CAPITAL_BEGIN = new KPIModel('CAPITAL_BEGIN', 'Capital account, beginning balance', DATA_TYPE.MONETARY, true,
      null, CAPITAL_BEGIN_FORMULA);
  var CONTRIBUTED_CAPITAL = new KPIModel('CONTRIBUTED_CAPITAL', 'Contributed capital during the fiscal period',
      DATA_TYPE.MONETARY, true, null, CONTRIBUTED_CAPITAL_FORMULA);
  var DRAWS = new KPIModel('DRAWS', 'Draws', DATA_TYPE.MONETARY, true, null, DRAWS_FORMULA);
  var INCOME_ALLOCATION = new KPIModel('INCOME_ALLOCATION', 'Income allocation', DATA_TYPE.MONETARY, true, null,
      INCOME_ALLOCATION_FORMULA);
  var components = [CAPITAL_BEGIN, CONTRIBUTED_CAPITAL, DRAWS, INCOME_ALLOCATION];
  var componentDef = new ComponentDefinition('rysMS6zh0', 'Total', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
      wpw.analysis.modelsToKeys(components), null, true);
  equityDef.addUniqueKpiModels(components);
  equityDef.componentDefinitions.push(componentDef);

  // Accounts
  componentDef = new ComponentDefinition('BJfe8EMzx', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  componentDef.allowEdit = false;
  componentDef.breakdownGroupNumbers = ['1.5.200'];
  equityDef.componentDefinitions.push(componentDef);

  wpw.injectedAnalysisDefinitions.push(equityDef);
})();
