(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var REVENUE_FORMULA = TAG('2.4');
  var COS_FORMULA = TAG('2.5');
  var SALES_FORMULA = ADD(TAG('2.4.100'), TAG('2.4.110'), TAG('2.4.200'));
  var LAST_YEAR_SALES_FORMULA = ADD(TAG('2.4.100', -1), TAG('2.4.110', -1));
  var GROSS_MARGIN_NUM_FORMULA = MINUS(REVENUE_FORMULA, COS_FORMULA);
  var GROSS_MARGIN_FORMULA = DIVIDE(MINUS(REVENUE_FORMULA, COS_FORMULA), REVENUE_FORMULA);
  var ACCOUNTS_RECEIVABLE_FORMULA = TAG('1.1.100');
  var INVENTORY_FORMULA = TAG('1.1.110');
  var ACCOUNTS_PAYABLE_FORMULA = TAG('1.3.215');
  var INVENTORY_TURNOVER_FORMULA = DIVIDE(COS_FORMULA, INVENTORY_FORMULA);
  var DAYS_COS_INVENTORY_FORMULA = DIVIDE(MULTIPLY(INVENTORY_FORMULA, CONSTANT(365)), COS_FORMULA);
  var OPERATING_CYCLE_DAYS_FORMULA = ADD(
      DIVIDE(MULTIPLY(CONSTANT(365), ACCOUNTS_RECEIVABLE_FORMULA), SALES_FORMULA), // days receivables
      DAYS_COS_INVENTORY_FORMULA);
  var INVENTORY_SALES_RATIO_FORMULA = DIVIDE(INVENTORY_FORMULA, SALES_FORMULA);
  var INVENTORY_GROWTH_FORMULA = DIVIDE(MINUS(TAG('1.1.110'), TAG('1.1.110', -1)), TAG('1.1.110', -1));
  var SALES_GROWTH_FORMULA = DIVIDE(MINUS(SALES_FORMULA, LAST_YEAR_SALES_FORMULA), LAST_YEAR_SALES_FORMULA);
  var FINISHED_GOOD_FORMULA = TAG('1.1.110.100');
  var INVENTORY_FOR_LT_FORMULA = TAG('1.1.110.105');
  var WORK_IN_PROGRESS_FORMULA = TAG('1.1.110.110');
  var RAW_MATERIALS_FORMULA = TAG('1.1.110.115');
  var OTHER_INVENTORIES_FORMULA = TAG('1.1.110.120');
  var INVENTORY_VALUATION_RESERVE_FORMULA = TAG('1.1.110.920');
  var LIFO_RESERVE_FORMULA = TAG('1.1.110.925');
  var WRITE_DOWN_INVENTORIES_FORMULA = TAG('2.5.100.121');
  /********************* Formulas ends **********************/

  var inventoryDef = new wpw.analysis.AnalysisDefinition();
  inventoryDef.id = 'yrFqmG9MTkKF6cOOenTebw';
  inventoryDef.name = 'Inventories';

  // Overview
  var SALES = new KPIModel('SALES', 'Sales', DATA_TYPE.MONETARY, true, null, SALES_FORMULA);
  var COS = new KPIModel('COS', 'Cost of sales', DATA_TYPE.MONETARY, true, null, COS_FORMULA);
  var GROSS_MARGIN_NUM = new KPIModel('GROSS_MARGIN_NUM', 'Gross margin', DATA_TYPE.NUMBER, false, null,
      GROSS_MARGIN_NUM_FORMULA, 'Revenue - Cost of sales');
  var INVENTORY = new KPIModel('INVENTORY', 'Inventory', DATA_TYPE.MONETARY, true, null, INVENTORY_FORMULA);
  var ACCOUNTS_PAYABLE = new KPIModel('ACCOUNTS_PAYABLE', 'Accounts payable', DATA_TYPE.MONETARY, true, null,
      ACCOUNTS_PAYABLE_FORMULA);
  var componentModels = [SALES, COS, GROSS_MARGIN_NUM, INVENTORY, ACCOUNTS_PAYABLE];
  var component = new ComponentDefinition('rytN9YB2T', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  inventoryDef.kpiModels = inventoryDef.kpiModels.concat(componentModels);
  inventoryDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('HybLyge-ze', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = [INVENTORY_FORMULA.tagNumber];
  component.allowEdit = false;
  inventoryDef.componentDefinitions.push(component);

  // Ratios
  var INVENTORY_TURNOVER = new KPIModel('INVENTORY_TURNOVER', 'Inventory turnover', DATA_TYPE.NUMBER, false, null,
    INVENTORY_TURNOVER_FORMULA, 'Cost of sales / Inventory', 'Measures the number of times inventory turns over ' +
      'during the year. High turnover can indicate better liquidity or superior merchandising. Conversely, it can ' +
      'indicate a shortage of needed inventory for the level of sales. Low turnover can indicate poor liquidity, ' +
      'overstocking, or obsolescence.');
  INVENTORY_TURNOVER.numberOfDecimals = 2;
  var DAYS_COS_INVENTORY = new KPIModel('DAYS_COS_INVENTORY', 'Days cost of sales in inventory', DATA_TYPE.NUMBER,
    false, null, DAYS_COS_INVENTORY_FORMULA, '(Inventory X 365) / Cost of Sales', 'Measures the average length of ' +
      'time units are in inventory. Helps determine if inadequate or excess inventory levels exist.');
  var OPERATING_CYCLE_DAYS = new KPIModel('OPERATING_CYCLE_DAYS', 'Operating cycle days', DATA_TYPE.NUMBER, false,
      null, OPERATING_CYCLE_DAYS_FORMULA, 'Days Sales in Inventory / Days cost of Sales in Inventory', 'Measures the ' +
      'time it takes to convert products and services into cash. An unfavorable trend may be a leading indicator of ' +
      'future cash flow problems.');
  var GROSS_MARGIN = new KPIModel('GROSS_MARGIN', 'Gross margin', DATA_TYPE.PERCENTAGE, false, null,
      GROSS_MARGIN_FORMULA, '(Revenue - Cost of sales) / Revenue');
  var INVENTORY_SALES_RATIO = new KPIModel('INVENTORY_SALES_RATIO', 'Inventory / Sales', DATA_TYPE.PERCENTAGE, false,
      null, INVENTORY_SALES_RATIO_FORMULA, 'Inventory / Sales');
  var INVENTORY_GROWTH = new KPIModel('INVENTORY_GROWTH', 'Inventory growth', DATA_TYPE.PERCENTAGE, false, null,
      INVENTORY_GROWTH_FORMULA, '');
  var SALES_GROWTH = new KPIModel('SALES_GROWTH', 'Sales growth', DATA_TYPE.PERCENTAGE, false, null,
      SALES_GROWTH_FORMULA, '');
  componentModels = [INVENTORY_TURNOVER, DAYS_COS_INVENTORY, OPERATING_CYCLE_DAYS, GROSS_MARGIN, INVENTORY_SALES_RATIO,
    INVENTORY_GROWTH, SALES_GROWTH];
  component = new ComponentDefinition('SkqN5FrhT', 'Ratios', COMPONENT_TYPES.TABLE, null,
      wpw.analysis.modelsToKeys(componentModels));
  inventoryDef.addUniqueKpiModels(componentModels);
  inventoryDef.componentDefinitions.push(component);

  // Details - Inventory
  var FINISHED_GOOD = new KPIModel('FINISHED_GOOD', 'Finished goods', DATA_TYPE.MONETARY, true, null,
      FINISHED_GOOD_FORMULA);
  var INVENTORY_FOR_LT = new KPIModel('INVENTORY_FOR_LT', 'Inventory for long term contracts', DATA_TYPE.MONETARY, true,
      null, INVENTORY_FOR_LT_FORMULA);
  var WORK_IN_PROGRESS = new KPIModel('WORK_IN_PROGRESS', 'Work in progress', DATA_TYPE.MONETARY, true, null,
      WORK_IN_PROGRESS_FORMULA);
  var RAW_MATERIALS = new KPIModel('RAW_MATERIALS', 'Raw materials', DATA_TYPE.MONETARY, true, null,
      RAW_MATERIALS_FORMULA);
  var OTHER_INVENTORIES = new KPIModel('OTHER_INVENTORIES', 'Other inventories', DATA_TYPE.MONETARY, true, null,
      OTHER_INVENTORIES_FORMULA);
  var INVENTORY_VALUATION_RESERVE = new KPIModel('INVENTORY_VALUATION_RESERVE', 'Inventory valuation reserve',
      DATA_TYPE.MONETARY, true, null, INVENTORY_VALUATION_RESERVE_FORMULA);
  var LIFO_RESERVE = new KPIModel('LIFO_RESERVE', 'LIFO reserve', DATA_TYPE.MONETARY, true, null, LIFO_RESERVE_FORMULA);
  var WRITE_DOWN_INVENTORY = new KPIModel('WRITE_DOWN_INVENTORY', 'Write-down of inventories to net realizable value',
      DATA_TYPE.MONETARY, true, null, WRITE_DOWN_INVENTORIES_FORMULA);
  componentModels = [FINISHED_GOOD, INVENTORY_FOR_LT, WORK_IN_PROGRESS, RAW_MATERIALS, OTHER_INVENTORIES,
    INVENTORY_VALUATION_RESERVE, WRITE_DOWN_INVENTORY];
  component = new ComponentDefinition('ByiEqtrn6', 'Inventory', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels), null, true, true);
  inventoryDef.kpiModels = inventoryDef.kpiModels.concat(componentModels);
  inventoryDef.componentDefinitions.push(component);

  // Details - B
  inventoryDef.rootTagsToTraverse[COS_FORMULA.tagNumber] = COS.key;
  component = new ComponentDefinition('ry3NqFH2a', 'Cost of Sales', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, [COS.key], null, true, true);
  component.extraGroupsToLoad.push({key: COS.key});
  inventoryDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(inventoryDef);
})(wpw);
