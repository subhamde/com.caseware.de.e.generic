(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  var REVENUE_FORMULA = TAG('2.4');
  var TOTAL_ASSETS_FORMULA = ADD(TAG('1.1'), TAG('1.2'));
  var TOTAL_LIABILITIES_FORMULA = ADD(TAG('1.3'), TAG('1.4'));
  var TOTAL_EQUITY_FORMULA = TAG('1.5');
  var TOTAL_COS_FORMULA = TAG('2.5');
  var TOTAL_EXPENSE_FORMULA = TAG('2.6');
  var INCOME_TAX_FORMULA = TAG('2.7.600');
  var INCOME_TAX_RATIO_FORMULA = DIVIDE(INCOME_TAX_FORMULA, REVENUE_FORMULA);
  // Sales = Sales revenue, good + Sales revenue, services
  var TOTAL_SALES = ADD(TAG('2.4.100'), TAG('2.4.110'));
  // Revenue - COS - Expenses - Nonoperating income(expense) - Income tax expense(benefit)
  var NET_INCOME_FORMULA = TAG('net_income');
  var NET_INCOME_RATIO_FORMULA = DIVIDE(NET_INCOME_FORMULA, REVENUE_FORMULA);
  // EBIT = Net Income + Income tax expense(benefit) + Interest and debt expense
  var EBIT_RATIO_FORMULA = DIVIDE(ADD(NET_INCOME_FORMULA, INCOME_TAX_FORMULA, TAG('2.7.200')), REVENUE_FORMULA);
  // EBITDA = EBIT + Depreciation of property, plant and equipment (both under Expenses & CoS) +
  //                  Amortization of goodwill + Amortization of other intangible assets (both under Expenses & CoS)
  var EBITDA_FORMULA = DIVIDE(ADD(NET_INCOME_FORMULA, INCOME_TAX_FORMULA, TAG('2.7.200'),
      TAG('2.5.100.103'), TAG('2.6.500.100'), TAG('2.6.500.102'), TAG('2.5.100.107'), TAG('2.6.500.105')), REVENUE_FORMULA);
  // Revenue / Sales
  var REVENUE_SALES_RATIO_FORMULA = CONSTANT(1);
  // (Revenue - Cost of sales) / Revenue
  var GROSS_MARGIN_FORMULA = DIVIDE(MINUS(REVENUE_FORMULA, TAG('2.5')), REVENUE_FORMULA);
  var EXPENSES_RATIO_FORMULA = DIVIDE(TAG('2.6'), REVENUE_FORMULA);
  var COS_RATIO_FORMULA = DIVIDE(TOTAL_COS_FORMULA, REVENUE_FORMULA);
  // Current assets / Current liabilities
  var CURRENT_RATIO_FORMULA = DIVIDE(TAG('1.1'), TAG('1.3'));
  // Current assets - Current liabilities
  var WORKING_CAPITAL_FORMULA = MINUS(TAG('1.1'), TAG('1.3'));
  // Quick Assets / Current liabilities (Quick Assets = Cash and cash equivalents + Short term investments + Accounts and other receivables)
  var QUICK_RATIO_FORMULA = DIVIDE(ADD(TAG('1.1.000'), TAG('1.1.050'), TAG('1.1.100')), TAG('1.3'));
  // (Cash and cash equivalents + Short term investments ) / Current liabilities
  var CASH_RATIO_FORMULA = DIVIDE(ADD(TAG('1.1.000'), TAG('1.1.050')), TAG('1.3'));
  // Gross profit margin = (Revenue - Cost of sales) / Total Sales
  var GROSS_PROFIT_MARGIN_FORMULA = DIVIDE(MINUS(REVENUE_FORMULA, TAG('2.5')), REVENUE_FORMULA);
  // Net profit margin = Net Income / Total Sales
  var NET_PROFIT_MARGIN_FORMULA  = DIVIDE(NET_INCOME_FORMULA, TOTAL_SALES);
  // Return on assets = Net Income / Total Assets
  var RETURN_ON_ASSETS_FORMULA = DIVIDE(NET_INCOME_FORMULA, TOTAL_ASSETS_FORMULA);
  // Pre-tax on equity = (Net Income + Tax) / Equity
  var PRE_TAX_ON_EQUITY_FORMULA = DIVIDE(ADD(NET_INCOME_FORMULA, INCOME_TAX_FORMULA), TOTAL_EQUITY_FORMULA);
  // After-tax return on equity = Net Income / Equity
  var AFTER_TAX_ON_EQUITY_FORMULA = DIVIDE(NET_INCOME_FORMULA, TOTAL_EQUITY_FORMULA);
  /********* Formulas end ********/

  /************ Overall Starts ***********/
  var overallDefinition = new wpw.analysis.AnalysisDefinition();
  overallDefinition.id = 'RwdlmRYES828EEUV7ypiRw';
  overallDefinition.name = 'Overall';

  // Overall chart
  var TOTAL_ASSETS = new KPIModel('TOTAL_ASSETS', 'Total assets', DATA_TYPE.MONETARY, true, null, TOTAL_ASSETS_FORMULA);
  var TOTAL_LIABILITIES = new KPIModel('TOTAL_LIABILITIES', 'Total liabilities', DATA_TYPE.MONETARY, true, null,
    TOTAL_LIABILITIES_FORMULA);
  var TOTAL_EQUITY = new KPIModel('TOTAL_EQUITY', 'Total equity', DATA_TYPE.MONETARY, true, null, TOTAL_EQUITY_FORMULA);
  var TOTAL_REVENUE = new KPIModel('TOTAL_REVENUE', 'Total revenue', DATA_TYPE.MONETARY, true,
      wpw.analysis.KPIModel.BRIGHT_COLORS[0], REVENUE_FORMULA);
  var TOTAL_COS = new KPIModel('TOTAL_COS', 'Total cost of sales', DATA_TYPE.MONETARY, true, null, TOTAL_COS_FORMULA);
  var TOTAL_EXPENSE = new KPIModel('TOTAL_EXPENSE', 'Total expense', DATA_TYPE.MONETARY, true, null,
      TOTAL_EXPENSE_FORMULA);
  var NET_INCOME = new KPIModel('NET_INCOME', 'Net income (loss)', DATA_TYPE.MONETARY, false, null, NET_INCOME_FORMULA);
  overallDefinition.addUniqueKpiModels([TOTAL_ASSETS, TOTAL_LIABILITIES, TOTAL_EQUITY, TOTAL_REVENUE, TOTAL_COS,
      TOTAL_EXPENSE, NET_INCOME]);
  var overallChart = new ComponentDefinition('rkl6yoBna', 'Overall', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, [TOTAL_ASSETS.key, TOTAL_LIABILITIES.key, TOTAL_EQUITY.key, TOTAL_REVENUE.key,
        TOTAL_COS.key, TOTAL_EXPENSE.key, NET_INCOME.key]);
  overallDefinition.componentDefinitions.push(overallChart);

  // Overall - Relationships
  var REVENUE_RATIO = new KPIModel('REVENUE_RATIO', 'Revenue', DATA_TYPE.PERCENTAGE, true,
      wpw.analysis.KPIModel.BRIGHT_COLORS[0], REVENUE_SALES_RATIO_FORMULA);
  var COS_RATIO = new KPIModel('COS_RATIO', 'Cost of sales', DATA_TYPE.PERCENTAGE, false, null, COS_RATIO_FORMULA);
  var GROSS_MARGIN_RATIO = new KPIModel('GROSS_MARGIN', 'Gross margin', DATA_TYPE.PERCENTAGE, false, null,
    GROSS_MARGIN_FORMULA, '(Revenue - Cost of sales) / Revenue');
  var EXPENSES_RATIO = new KPIModel('EXPENSES_RATIO', 'Expenses', DATA_TYPE.PERCENTAGE, false, null,
      EXPENSES_RATIO_FORMULA);
  var NET_INCOME_RATIO = new KPIModel('NET_INCOME_RATIO', 'Net income (loss)', DATA_TYPE.PERCENTAGE, false, null,
      NET_INCOME_RATIO_FORMULA);
  var TAX_RATIO = new KPIModel('TAX_RATIO', 'Income tax expense (benefit)', DATA_TYPE.PERCENTAGE, false, null,
      INCOME_TAX_RATIO_FORMULA);
  var EBIT = new KPIModel('EBIT', 'EBIT', DATA_TYPE.PERCENTAGE, false, null, EBIT_RATIO_FORMULA, 'Net Income ' +
      '(Loss) + Income Tax Expense (Benefit) + Interest and Debt Expense', 'Earnings Before Interest and Tax');
  var EBITDA = new KPIModel('EBITDA', 'EBITDA', DATA_TYPE.PERCENTAGE, false, null, EBITDA_FORMULA, 'EBIT + ' +
      'Depreciation of property, plant and equipment (both under Expenses & CoS) + Amortization of goodwill + ' +
      'Amortization of other intangible assets (both under Expenses & CoS)', 'Earnings Before Interest, Tax, ' +
      'Depreciation, Amortization');
  overallDefinition.kpiModels.push(REVENUE_RATIO, COS_RATIO, GROSS_MARGIN_RATIO, EXPENSES_RATIO, NET_INCOME_RATIO,
    TAX_RATIO, EBIT, EBITDA);
  var relationsChart = new ComponentDefinition('rybpyiS36', 'Relationships', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE, [REVENUE_RATIO.key, GROSS_MARGIN_RATIO.key, COS_RATIO.key,
        EXPENSES_RATIO.key, NET_INCOME_RATIO.key, TAX_RATIO.key, EBIT.key, EBITDA.key], null, true);
  overallDefinition.componentDefinitions.push(relationsChart);

  // Overall - Liquidity Ratio
  var CURRENT_RATIO = new KPIModel('CURRENT_RATIO', 'Current ratio', DATA_TYPE.PERCENTAGE, false, null,
      CURRENT_RATIO_FORMULA, 'Current Assets / Current Liabilities', 'The Current Ratio measures a company\'s ' +
      'short-term liquidity and its ability to meet current obligations when due. The higher the ratio, the greater ' +
      'the cushion between current obligations and the company\'s ability to pay them.');
  var WORKING_CAPITAL = new KPIModel('WORKING_CAPITAL', 'Working capital', DATA_TYPE.MONETARY, false, null,
      WORKING_CAPITAL_FORMULA, 'Current Assets - Current Liabilities', 'The Working Capital Ratio measures the ' +
      'excess of current assets over current liabilities. It denotes the amount of assets expected to be converted ' +
      'to cash or consumed within one year beyond those used to pay current obligations. The amount is useful in ' +
      'performing a trend analysis on the company.');
  var QUICK_RATIO = new KPIModel('QUICK_RATIO', 'Quick ratio', DATA_TYPE.PERCENTAGE, false, null, QUICK_RATIO_FORMULA,
      'Quick Assets / Current Liabilities', 'The Quick Ratio (also known as the Acid-Test) measures the degree to ' +
      'which current obligations are covered by the most liquid current assets. Usually a value less than two implies' +
      ' a dependency on inventory or other current assets to liquidate short-term debt.');
  var CASH_RATIO = new KPIModel('CASH_RATIO', 'Cash ratio', DATA_TYPE.PERCENTAGE, false, null, CASH_RATIO_FORMULA,
      'Cash and Cash equivalents / Current Liabilities', 'The cash ratio is used to determine whether a business can ' +
      'meet its short-term obligations.');
  overallDefinition.kpiModels.push(CURRENT_RATIO, WORKING_CAPITAL, QUICK_RATIO, CASH_RATIO);
  overallDefinition.componentDefinitions.push(new ComponentDefinition('Syfp1sB3a', 'Liquidity Ratios',
      COMPONENT_TYPES.CHART, CHART_TYPES.LINE_COLUMN, [CURRENT_RATIO.key, WORKING_CAPITAL.key, QUICK_RATIO.key,
        CASH_RATIO.key], null, true, true));

  // Overall - Profitability Ratio
  var GROSS_PROFIT_MARGIN = new KPIModel('GROSS_PROFIT_MARGIN', 'Gross profit margin', DATA_TYPE.PERCENTAGE, false,
      null, GROSS_PROFIT_MARGIN_FORMULA, 'Gross Profit / Total Sales', 'Indicates how much of each sales currency ' +
      'is available to cover operating expense and contribute to profit.');
  var NET_PROFIT_MARGIN = new KPIModel('NET_PROFIT_MARGIN', 'Net profit margin', DATA_TYPE.PERCENTAGE, false, null,
      NET_PROFIT_MARGIN_FORMULA, 'Net Profit / Total Sales', 'Measures how much of each currency of sales was ' +
      'converted to profit.');
  var RETURN_ON_ASSETS = new KPIModel('RETURN_ON_ASSETS', 'Return on assets', DATA_TYPE.PERCENTAGE, false, null,
      RETURN_ON_ASSETS_FORMULA, 'Net Income (Loss) / Total Assets', 'Measures the ability to generate profits by additional ' +
      'investment in productive assets.');
  var PRE_TAX_ON_EQUITY = new KPIModel('PRE_TAX_ON_EQUITY', 'Pre-tax return on equity', DATA_TYPE.PERCENTAGE, false,
      null, PRE_TAX_ON_EQUITY_FORMULA, 'Net Income (Loss) + Tax / Equity', 'Measures each sales currency’s pre-tax benefits' +
      ' to owners.');
  var AFTER_TAX_ON_EQUITY = new KPIModel('AFTER_TAX_ON_EQUITY', 'After-tax return on equity', DATA_TYPE.PERCENTAGE,
      false, null, AFTER_TAX_ON_EQUITY_FORMULA, 'Net Income (Loss) / Equity', 'Measures each sales currency’s benefits to ' +
      'owners.');
  overallDefinition.kpiModels.push(GROSS_PROFIT_MARGIN, NET_PROFIT_MARGIN, RETURN_ON_ASSETS, PRE_TAX_ON_EQUITY,
    AFTER_TAX_ON_EQUITY);
  overallDefinition.componentDefinitions.push(new ComponentDefinition('r17ayjSna',
      'Profitability Ratios', COMPONENT_TYPES.CHART, CHART_TYPES.LINE_COLUMN, [GROSS_PROFIT_MARGIN.key,
      NET_PROFIT_MARGIN.key, RETURN_ON_ASSETS.key, PRE_TAX_ON_EQUITY.key, AFTER_TAX_ON_EQUITY.key], null, true));

  // Charts of Tag
  overallDefinition.rootTagsToTraverse['1.1'] = TOTAL_ASSETS.key;
  overallDefinition.rootTagsToTraverse['1.2'] = TOTAL_ASSETS.key;
  overallDefinition.rootTagsToTraverse['1.3'] = TOTAL_LIABILITIES.key;
  overallDefinition.rootTagsToTraverse['1.4'] = TOTAL_LIABILITIES.key;
  overallDefinition.rootTagsToTraverse['1.5'] = TOTAL_EQUITY.key;
  overallDefinition.rootTagsToTraverse['2.4'] = TOTAL_REVENUE.key;
  overallDefinition.rootTagsToTraverse['2.5'] = TOTAL_COS.key;
  overallDefinition.rootTagsToTraverse['2.6'] = TOTAL_EXPENSE.key;
  overallDefinition.componentDefinitions.push(new ComponentDefinition('B1VpJiS3p', 'Assets',
      COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], TOTAL_ASSETS.key, true, true));
  overallDefinition.componentDefinitions.push(new ComponentDefinition('HkH6JsB26', 'Liabilities',
      COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], TOTAL_LIABILITIES.key, true, true));
  overallDefinition.componentDefinitions.push(new ComponentDefinition('ByIpksB36', 'Equity',
      COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], TOTAL_EQUITY.key, true, true));
  overallDefinition.componentDefinitions.push(new ComponentDefinition('Hyvp1ir3p', 'Revenue',
      COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], TOTAL_REVENUE.key, true, true));
  overallDefinition.componentDefinitions.push(new ComponentDefinition('BkOayjH26', 'Cost of Sales',
      COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], TOTAL_COS.key, true, true));
  overallDefinition.componentDefinitions.push(new ComponentDefinition('SyYa1irha', 'Expenses',
      COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], TOTAL_EXPENSE.key, true, true));
  /************ Overall ends ***********/

  overallDefinition.defaultComponentIds = [overallChart.key, relationsChart.key];
  wpw.injectedAnalysisDefinitions.push(overallDefinition);
})(wpw);
