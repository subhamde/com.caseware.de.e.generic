(function(wpw) {
  'use strict';

  var CORPORATION_CHECKLIST_ID = '0Xtk9-iaTAu_2qnq77uftA';
  var CORPORATION_PROCEDURE_ID = 'eZiFN4BBSQKMQlFaAY5-Bw';
  var CORPORATION_RESPONSE_ID = 'zyg5t3WyTZOEqC8zuV-q2Q';

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  var CAPITAL_STOCK_FORMULA = TAG('1.5.100.100');
  var RETAINED_EARNINGS_FORMULA = TAG('1.5.100.200');
  var OTHER_CAPITAL_FORMULA = TAG('1.5.100.200.800');
  var NET_INCOME_FORMULA = TAG('NETINC');
  var DIVIDENDS_FORMULA = ADD(TAG('1.5.100.200.600'), TAG('1.5.100.200.700'));
  /************ Formulas end *****/

  var equityDef = new wpw.analysis.AnalysisDefinition();
  equityDef.id = 'ni8qLnG4TyeKYqMTsOeuBw';
  equityDef.name = 'Equity - Corporate';

  // Visible only when engagement is Corporate. Determined by answer from checklist.
  equityDef.visibilitySetting = new wpw.models.Visibility.Settings();
  equityDef.visibilitySetting.allConditionsNeeded = true;
  equityDef.visibilitySetting.normallyVisible = false;
  var isCorpCond = new wpw.models.Visibility.ResponseCondition(CORPORATION_CHECKLIST_ID);
  isCorpCond.procedureId = CORPORATION_PROCEDURE_ID;
  isCorpCond.responseId = CORPORATION_RESPONSE_ID;
  equityDef.visibilitySetting.conditions.push(isCorpCond);

  // Overview
  equityDef.rootTagsToTraverse[CAPITAL_STOCK_FORMULA.tagNumber] = 'CAPITAL_STOCK';
  var RETAINED_EARNINGS = new KPIModel('RETAINED_EARNINGS', 'Retained earnings', DATA_TYPE.MONETARY, true, null,
      RETAINED_EARNINGS_FORMULA);
  equityDef.rootTagsToTraverse[OTHER_CAPITAL_FORMULA.tagNumber] = 'OTHER_CAPITAL';
  var NET_INCOME = new KPIModel('NET_INCOME', 'Net income', DATA_TYPE.MONETARY, true, null, NET_INCOME_FORMULA);
  var DIVIDENDS = new KPIModel('DIVIDENDS', 'Dividends', DATA_TYPE.MONETARY, true, null, DIVIDENDS_FORMULA);
  var component = new ComponentDefinition('rJzfiu2AC', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
      ['CAPITAL_STOCK', RETAINED_EARNINGS.key, 'OTHER_CAPITAL', NET_INCOME.key, DIVIDENDS.key]);
  component.addExtraStackingGroup('CAPITAL_STOCK', false);
  component.addExtraStackingGroup('OTHER_CAPITAL', false);
  equityDef.kpiModels.push(RETAINED_EARNINGS, NET_INCOME, DIVIDENDS);
  equityDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('rkMnAszMMe', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['1.5.100.100', '1.5.100.200', '1.5.100.400', '1.5.100.500'];
  equityDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(equityDef);
})(wpw);
