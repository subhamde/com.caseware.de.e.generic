(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  var NET_PPE_FORMULA = TAG('1.2.120');
  var PPE_COST_FORMULA = TAG('1.2.120.100'); // Also Gross PPE
  var PPE_AD_FORMULA = TAG('1.2.120.500');
  var DEP_PPE_COS_FORMULA = TAG('2.5.100.103');
  var DEP_PPE_EXP_FORMULA = TAG('2.6.500.100');
  var IMPAIR_PPE_COS_FORMULA = TAG('2.5.100.104');
  var IMPAIR_PPE_EXP_FORMULA = TAG('2.6.500.110');
  var DEPLETABLE_COS_FORMULA = TAG('1.2.122.100');
  var DEPLETABLE_AD_FORMULA = TAG('1.2.122.200');
  var DEPLETION_FORMULA = TAG('2.6.500.123');
  var RM_FORMULA = TAG('2.6.505.285');
  var GAIN_LOSS_DISPOSITION_FORMULA = TAG('2.7.100.100');
  var RM_TO_GROSS_DEP_PPE_FORMULA = DIVIDE(RM_FORMULA, PPE_COST_FORMULA);
  var DEP_EXP_TO_GROSS_DEP_PPE_FORMULA = DIVIDE(DEP_PPE_EXP_FORMULA, PPE_COST_FORMULA);
  var AD_TO_GROSS_DEP_PPE_FORMULA = DIVIDE(PPE_AD_FORMULA, PPE_COST_FORMULA);
  var TOTAL_ASSETS_FORMULA = ADD(TAG('1.1'), TAG('1.2'));
  var GROSS_PPE_TO_TOTAL_ASSETS_FORMULA = DIVIDE(PPE_COST_FORMULA, TOTAL_ASSETS_FORMULA);
  var NET_SALES = ADD(TAG('2.4.100'), TAG('2.4.110'), TAG('2.4.200'));
  var SALES_TO_NET_PPE_FORMULA = DIVIDE(NET_SALES, NET_PPE_FORMULA);
  // Cost - AD
  var BOOK_VALUE_RATIO_FORMULA = DIVIDE(MINUS(PPE_COST_FORMULA, PPE_AD_FORMULA), PPE_COST_FORMULA);
  /********* Formulas end ********/
  var ppeDefinition = new wpw.analysis.AnalysisDefinition();
  ppeDefinition.id = 'U4dYPIziSpG37Zp1sY5JHw';
  ppeDefinition.name = 'Property, plant and equipment';

  var componentModels;
  var component;

  // PPE/Accumulated Dep/Expenses
  var PPE_COST = new KPIModel('PPE_COST', 'Property, plant and equipment - Cost', DATA_TYPE.MONETARY, true, null,
    PPE_COST_FORMULA);
  var PPE_AD = new KPIModel('PPE_AD', 'Property, plant and equipment - A/D and impairment', DATA_TYPE.MONETARY, true,
    null, PPE_AD_FORMULA);
  var DEP_PPE_COS = new KPIModel('DEP_PPE_COS', 'Depreciation of PPE, Cost of sales', DATA_TYPE.MONETARY, true, null,
     DEP_PPE_COS_FORMULA);
  var DEP_PPE_EXP = new KPIModel('DEP_PPE_EXP', 'Depreciation of PPE, Expense', DATA_TYPE.MONETARY, true, null,
    DEP_PPE_EXP_FORMULA);
  var IMPAIR_PPE_COS = new KPIModel('IMPAIR_PPE_COS', 'Impairment of PPE, Cost of sales', DATA_TYPE.MONETARY, true,
    null, IMPAIR_PPE_COS_FORMULA);
  var IMPAIR_PPE_EXP = new KPIModel('IMPAIR_PPE_EXP', 'Impairment of PPE, Expense', DATA_TYPE.MONETARY, true,
    null, IMPAIR_PPE_EXP_FORMULA);
  var DEPLETABLE_COS = new KPIModel('DEPLETABLE_COS', 'Depletable assets', DATA_TYPE.MONETARY, true, null,
      DEPLETABLE_COS_FORMULA);
  var DEPLETABLE_AD = new KPIModel('DEPLETABLE_AD', 'Depletable assets, Accumulated depletion', DATA_TYPE.MONETARY,
      true, null, DEPLETABLE_AD_FORMULA);
  var DEPLETION = new KPIModel('DEPLETION', 'Depletion', DATA_TYPE.MONETARY, true, null, DEPLETION_FORMULA);
  var RM = new KPIModel('R&M', 'Repairs and maintenance', DATA_TYPE.MONETARY, true, null, RM_FORMULA);
  var GAIN_LOSS_DISPOSITION = new KPIModel('Sy96yir26', 'Gain(loss) on disposition', DATA_TYPE.MONETARY,
      true, null, GAIN_LOSS_DISPOSITION_FORMULA);
  componentModels = [PPE_COST, PPE_AD, DEP_PPE_COS, DEP_PPE_EXP, IMPAIR_PPE_COS, IMPAIR_PPE_EXP, DEPLETABLE_COS,
    DEPLETABLE_AD, DEPLETION, RM, GAIN_LOSS_DISPOSITION];
  ppeDefinition.kpiModels = ppeDefinition.kpiModels.concat(componentModels);
  component = new ComponentDefinition('BJyl6kiH2p', 'Overview',
    COMPONENT_TYPES.CHART, CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('DEP_PPE', [DEP_PPE_COS, DEP_PPE_EXP], false, 3);
  component.groupKeysInStack('IMPAIR_PPE', [IMPAIR_PPE_COS, IMPAIR_PPE_EXP], false, 4);
  ppeDefinition.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('HkQUJglbGe', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['1.2.120', '1.2.122'];
  ppeDefinition.componentDefinitions.push(component);

  // Ratio
  var RM_TO_GROSS_DEP_PPE = new KPIModel('RM_TO_GROSS_DEP_PPE', 'Repairs & maintenance / Gross depreciable PPE',
    DATA_TYPE.PERCENTAGE, false, null, RM_TO_GROSS_DEP_PPE_FORMULA, 'Repairs & maintenance / Gross depreciable PPE',
      'Expresses the relationship of repairs and maintenance to gross property and equipment. Percentages outside of ' +
      'normal ranges could indicate classification errors in amounts capitalised. High percentages can indicate ' +
      'older property and equipment with needs replacement.');
  var DEP_EXP_TO_GROSS_DEP_PPE = new KPIModel('DEP_EXP_TO_GROSS_DEP_PPE',
    'Depreciation expense / Gross depreciable PPE', DATA_TYPE.PERCENTAGE, false, null,
    DEP_EXP_TO_GROSS_DEP_PPE_FORMULA, 'Depreciation expense / Gross depreciable PPE', 'Measures the rate at which ' +
      'productive asset costs are allocated to operations.');
  var AD_TO_GROSS_DEP_PPE = new KPIModel('AD_TO_GROSS_DEP_PPE', 'Accumulated depreciation / Gross depreciable PPE',
    DATA_TYPE.PERCENTAGE, false, null, AD_TO_GROSS_DEP_PPE_FORMULA, 'Accumulated depreciation / Gross depreciable PPE',
      'Shows the cumulative percentage of productive asset costs allocated to operations. A high percentage usually ' +
      'indicates older property and equipment. Significant capital investment may be required in future periods.');
  var GROSS_PPE_TO_TOTAL_ASSETS = new KPIModel('GROSS_PPE_TO_TOTAL_ASSETS', 'Gross PPE / Total Assets',
    DATA_TYPE.PERCENTAGE, false, null, GROSS_PPE_TO_TOTAL_ASSETS_FORMULA, 'Gross PPE / Total Assets');
  var FIXED_ASSETS_TURNOVER = new KPIModel('FIXED_ASSETS_TURNOVER', 'Fixed assets turnover', DATA_TYPE.NUMBER,
      false, null, SALES_TO_NET_PPE_FORMULA, 'Net Sales / Net PPE');
  FIXED_ASSETS_TURNOVER.numberOfDecimals = 2;
  var BOOK_VALUE_RATIO = new KPIModel('BOOK_VALUE_RATIO', 'Book value ratio', DATA_TYPE.PERCENTAGE, false, null,
    BOOK_VALUE_RATIO_FORMULA, 'Net PPE / Cost PPE');
  componentModels = [RM_TO_GROSS_DEP_PPE, DEP_EXP_TO_GROSS_DEP_PPE, AD_TO_GROSS_DEP_PPE, GROSS_PPE_TO_TOTAL_ASSETS,
    FIXED_ASSETS_TURNOVER, BOOK_VALUE_RATIO];
  ppeDefinition.kpiModels = ppeDefinition.kpiModels.concat(componentModels);
  component = new ComponentDefinition('B1jTJjSn6', 'Ratios', COMPONENT_TYPES.TABLE, null,
    wpw.analysis.modelsToKeys(componentModels));
  ppeDefinition.componentDefinitions.push(component);

  // Detailed
  ppeDefinition.rootTagsToTraverse[PPE_COST_FORMULA.tagNumber] = PPE_COST.key;
  ppeDefinition.rootTagsToTraverse[PPE_AD_FORMULA.tagNumber] = PPE_AD.key;
  ppeDefinition.componentDefinitions.push(new ComponentDefinition('Sy3a1oB2T', 'PPE - Cost',
    COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], PPE_COST.key, true, true));
  ppeDefinition.componentDefinitions.push(new ComponentDefinition('HJpakjB3a',
    'PPE - Accumulated Depreciation and Impairment', COMPONENT_TYPES.CHART, CHART_TYPES.STACK, [], PPE_AD.key, true,
    true));
  ppeDefinition.matchGroupSettings.push(new wpw.analysis.MatchGroupSetting([PPE_COST_FORMULA.tagNumber,
    PPE_AD_FORMULA.tagNumber]));

  wpw.injectedAnalysisDefinitions.push(ppeDefinition);
})(wpw);
