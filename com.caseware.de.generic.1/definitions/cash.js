(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var CASH_FORMULA = TAG('1.1.000.050');
  var OTHER_CASH_AND_CASH_EQUIV_FORMULA = TAG('1.1.000.300');
  /********************* Formulas ends **********************/

  var analysisDef = new wpw.analysis.AnalysisDefinition();
  analysisDef.id = 'zLU3BwioQlaZct847TrSPw';
  analysisDef.name = 'Cash and cash equivalents';

  var CASH = new KPIModel('CASH', 'Cash', DATA_TYPE.MONETARY, true, null, CASH_FORMULA);
  var OTHER_CASH_AND_CASH_EQUIV = new KPIModel('OTHER_CASH_AND_CASH_EQUIV', 'Other cash and cash equivalents',
      DATA_TYPE.MONETARY, true, null, OTHER_CASH_AND_CASH_EQUIV_FORMULA);
  var component = new ComponentDefinition('B1ln_iCgzg', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
      [CASH.key, OTHER_CASH_AND_CASH_EQUIV.key]);
  analysisDef.addUniqueKpiModels([CASH, OTHER_CASH_AND_CASH_EQUIV]);
  analysisDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('BJ2uo0gfe', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['1.1.000'];
  component.allowEdit = false;
  analysisDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);
