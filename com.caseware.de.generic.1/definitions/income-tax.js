(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /************ Formulas *********/
  var NET_INCOME_FORMULA = TAG('net_income');
  var CURRENT_INCOME_TAX_EXPENSE_FORMULA = TAG('2.7.600.100');
  var DEFERRED_INCOME_TAX_EXPENSE_FORMULA = TAG('2.7.600.200');
  var INCOME_TAX_EXPENSE_FORMULA = ADD(CURRENT_INCOME_TAX_EXPENSE_FORMULA, DEFERRED_INCOME_TAX_EXPENSE_FORMULA);
  var INCOME_TAX_RECEIVABLE_FORMULA = TAG('1.1.104');
  var INCOME_TAXES_PAYABLE_FORMULA = TAG('1.3.217');
  var PROVISION_FOR_TAX_RISKS_FORMULA = ADD(TAG('1.3.265.300'), TAG('1.4.265.300'));
  var CUR_DEFERRED_TAX_LIAB_FORMULA = TAG('1.3.270');
  var NON_CUR_DEFERRED_TAX_LIAB_FORMULA = TAG('1.4.270');
  var CUR_DEFERRED_TAX_ASSETS_FORMULA = TAG('1.1.170');
  var NON_CUR_DEFERRED_TAX_ASSETS_FORMULA = TAG('1.2.170');
  var EFFECTIVE_TAX_RATE_FORMULA = DIVIDE(INCOME_TAX_EXPENSE_FORMULA, ADD(NET_INCOME_FORMULA,
      INCOME_TAX_EXPENSE_FORMULA));
  /*********** Formulas End ******/

  var incomeTaxDef = new wpw.analysis.AnalysisDefinition();
  incomeTaxDef.id = 'iQRAKC9aQQqz2OE4gQJyDQ';
  incomeTaxDef.name = 'Income taxes';

  // Overview
  var NET_INCOME = new KPIModel('NET_INCOME', 'Net income (loss)', DATA_TYPE.MONETARY, true, null, NET_INCOME_FORMULA);
  var CURRENT_INCOME_TAX_EXPENSE = new KPIModel('CURRENT_INCOME_TAX_EXPENSE',
      'Income tax expense (benefit), current', DATA_TYPE.MONETARY, true, null, CURRENT_INCOME_TAX_EXPENSE_FORMULA);
  var DEFERRED_INCOME_TAX_EXPENSE = new KPIModel('DEFERRED_INCOME_TAX_EXPENSE',
      'Income tax expense (benefit), deferred', DATA_TYPE.MONETARY, true, null, DEFERRED_INCOME_TAX_EXPENSE_FORMULA);
  var INCOME_TAX_RECEIVABLE = new KPIModel('INCOME_TAX_RECEIVABLE', 'Income taxes receivable', DATA_TYPE.MONETARY, true,
      null, INCOME_TAX_RECEIVABLE_FORMULA);
  var INCOME_TAXES_PAYABLE = new KPIModel('INCOME_TAXES_PAYABLE', 'Income taxes payable', DATA_TYPE.MONETARY, true,
      null, INCOME_TAXES_PAYABLE_FORMULA);
  var PROVISION_FOR_TAX_RISKS = new KPIModel('PROVISION_FOR_TAX_RISKS', 'Provision for tax risks', DATA_TYPE.MONETARY,
      true, null, PROVISION_FOR_TAX_RISKS_FORMULA);
  var CUR_DEFERRED_TAX_LIAB = new KPIModel('CUR_DEFERRED_TAX_LIAB', 'Deferred income tax liabilities - current',
      DATA_TYPE.MONETARY, true, null, CUR_DEFERRED_TAX_LIAB_FORMULA);
  var NON_CUR_DEFERRED_TAX_LIAB = new KPIModel('NON_CUR_DEFERRED_TAX_LIAB',
      'Deferred income tax liabilities - noncurrent', DATA_TYPE.MONETARY, true, null,
      NON_CUR_DEFERRED_TAX_LIAB_FORMULA);
  var CUR_DEFERRED_TAX_ASSETS = new KPIModel('CUR_DEFERRED_TAX_ASSETS', 'Deferred tax assets - current',
      DATA_TYPE.MONETARY, true, null, CUR_DEFERRED_TAX_ASSETS_FORMULA);
  var NON_CUR_DEFERRED_TAX_ASSETS = new KPIModel('NON_CUR_DEFERRED_TAX_ASSETS', 'Deferred tax assets - noncurrent',
      DATA_TYPE.MONETARY, true, null, NON_CUR_DEFERRED_TAX_ASSETS_FORMULA);
  var componentModels = [NET_INCOME, CURRENT_INCOME_TAX_EXPENSE, DEFERRED_INCOME_TAX_EXPENSE, INCOME_TAX_RECEIVABLE,
    INCOME_TAXES_PAYABLE, PROVISION_FOR_TAX_RISKS, CUR_DEFERRED_TAX_LIAB, NON_CUR_DEFERRED_TAX_LIAB,
    CUR_DEFERRED_TAX_ASSETS, NON_CUR_DEFERRED_TAX_ASSETS];
  incomeTaxDef.kpiModels = incomeTaxDef.kpiModels.concat(componentModels);
  var component = new ComponentDefinition('rJwE5FH3a', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('INCOME_TAX_EXP', [CURRENT_INCOME_TAX_EXPENSE, DEFERRED_INCOME_TAX_EXPENSE], false, 3);
  component.groupKeysInStack('INCOME_TAX_LIAB', [CUR_DEFERRED_TAX_LIAB, NON_CUR_DEFERRED_TAX_LIAB], false, 4);
  component.groupKeysInStack('TAX_ASSETS', [CUR_DEFERRED_TAX_ASSETS, NON_CUR_DEFERRED_TAX_ASSETS], false, 5);
  incomeTaxDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('SkIJelbzg', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['1.1.104', '1.1.170', '1.2.170', '1.3.217', '1.3.270', '1.4.270', '2.7.600'];
  component.allowEdit = false;
  incomeTaxDef.componentDefinitions.push(component);

  // Ratios
  var EFFECTIVE_TAX_RATE = new KPIModel('EFFECTIVE_TAX_RATE', 'Effective tax rate', DATA_TYPE.PERCENTAGE, false, null,
      EFFECTIVE_TAX_RATE_FORMULA, 'Income Tax Expense / Pretax Income', 'Measures the effective tax rate.');
  incomeTaxDef.kpiModels.push(EFFECTIVE_TAX_RATE);
  component = new ComponentDefinition('Sy_VcKr2p', 'Ratios', COMPONENT_TYPES.TABLE, null, [EFFECTIVE_TAX_RATE.key]);
  incomeTaxDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(incomeTaxDef);
})(wpw);
