(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var SALES_FORMULA = ADD(TAG('2.4.100'), TAG('2.4.110'), TAG('2.4.200'));
  var NET_SALES_FORMULA = SALES_FORMULA;
  var COS_FORMULA = TAG('2.5');
  // Selling expenses are mixed with admin in the template, so has to display them in one line.
  var SELLING_ADMIN_EXPENSES_FORMULA = TAG('2.6.505');
  var AP_FORMULA = TAG('1.3.215');
  var TOTAL_SUPPLIER_PURCHASE = ADD(
    TAG('2.5.100.050'),  // Opening inventory
    TAG('2.5.100.075'), // Purchases
    TAG('2.5.100.100'), // Direct materials
    TAG('2.5.100.102'), // Direct overhead
    TAG('2.5.100.990'), // Other costs
    TAG('2.5.100.999'), // Closing inventory
    TAG('2.6.500.135'), // Professional and other fees
    TAG('2.6.505.100'), // Advertising
    TAG('2.6.505.105'), // Professional fees
    TAG('2.6.505.260'), // Office expenses
    TAG('2.6.505.285'), // Repairs and maintenance
    TAG('2.6.505.290'), // Royalty, membership and license fees
    TAG('2.6.505.335'), // Transport and freight
    TAG('2.6.505.345'), // Travel
    TAG('2.6.505.350'), // Utilities
    TAG('2.6.505.375'), // Rentals
    TAG('2.6.505.990') // Other expenses
  );
  var AP_AVERAGE = DIVIDE(ADD(TAG('1.3.215.100', 0), TAG('1.3.215.100', -1)), CONSTANT(2));
  var AP_TURNOVER_FORMULA = DIVIDE(TOTAL_SUPPLIER_PURCHASE, AP_AVERAGE);
  var DAYS_COS_IN_PAYABLE_FORMULA = DIVIDE(MULTIPLY(AP_FORMULA, CONSTANT(365)), COS_FORMULA);
  var AP_TO_SALES_FORMULA = MULTIPLY(DIVIDE(AP_FORMULA, NET_SALES_FORMULA), CONSTANT(100));
  /********************* Formulas ends **********************/

  var pppDef = new wpw.analysis.AnalysisDefinition();
  pppDef.id = 'WKzCjUd5QQeOPgAAE7Xajw';
  pppDef.name = 'Purchases, Payables, Payments';

  var SALES = new KPIModel('SALES', 'Sales', DATA_TYPE.MONETARY, true, null, SALES_FORMULA);
  var COS = new KPIModel('COS', 'Cost of sales', DATA_TYPE.MONETARY, true, null, COS_FORMULA);
  var SELLING_ADMIN_EXPENSES = new KPIModel('SELLING_ADMIN_EXPENSES', 'Selling, general and administrative costs',
      DATA_TYPE.MONETARY, true, null, SELLING_ADMIN_EXPENSES_FORMULA);
  var AP = new KPIModel('AP', 'Accounts payable and accrued liabilities', DATA_TYPE.MONETARY, true, null, AP_FORMULA);
  var componentModels = [SALES, COS, SELLING_ADMIN_EXPENSES, AP];
  var component = new ComponentDefinition('PPP_OVERVIEW', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
    wpw.analysis.modelsToKeys(componentModels));
  pppDef.kpiModels = pppDef.kpiModels.concat(componentModels);
  pppDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('HJgh0ozMGg',  'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['1.3.215', '1.3.220', '1.3.225', '1.3.235', '1.4.225', '1.4.235'];
  pppDef.componentDefinitions.push(component);

  var AP_TURNOVER = new KPIModel('AP_TURNOVER', 'Accounts payable turnover', DATA_TYPE.NUMBER, false, null,
    AP_TURNOVER_FORMULA, 'Total supplier purchases / (Beginning accounts payable + Ending accounts payable) / 2)',
    'Accounts payable turnover is a ratio that measures the speed with which a company pays its suppliers.');
  var DAYS_COS_IN_PAYABLE = new KPIModel('DAYS_COS_IN_PAYABLE', 'Days cost of sales in payable', DATA_TYPE.NUMBER,
    false, null, DAYS_COS_IN_PAYABLE_FORMULA, '(Accounts Payable X 365) / Cost of Sales',
    'Measures the average age of accounts payable and indicates the bill paying pattern of the company.');
  var AP_TO_SALES = new KPIModel('AP_TO_SALES', 'Accounts payable to sales', DATA_TYPE.NUMBER, false, null,
    AP_TO_SALES_FORMULA, '(Accounts Payables / Net Sales) x 100',
    'Measures how much of suppliers money used to fund sales.');
  componentModels = [AP_TURNOVER, DAYS_COS_IN_PAYABLE, AP_TO_SALES];
  component = new ComponentDefinition('PPP_RATIOS', 'Ratios', COMPONENT_TYPES.TABLE, null,
    wpw.analysis.modelsToKeys(componentModels));
  pppDef.kpiModels = pppDef.kpiModels.concat(componentModels);
  pppDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(pppDef);
})(wpw);
