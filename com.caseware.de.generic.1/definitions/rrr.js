(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var MINUS = wpw.analysis.AnalysisFormula.FORMULAS.minus;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var MULTIPLY = wpw.analysis.AnalysisFormula.FORMULAS.multiply;
  var CONSTANT = wpw.analysis.AnalysisFormula.FORMULAS.constant;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;

  /************ Formulas *********/
  var AR_TAG_NUMBER = '1.1.100';
  var GROSS_AR_FORMULA = TAG(AR_TAG_NUMBER);
  var REVENUE_FORMULA = TAG('2.4');
  var RECEIVABLE_PER_FORMULA = DIVIDE(GROSS_AR_FORMULA, REVENUE_FORMULA);
  var AR_TURNOVER_FORMULA = DIVIDE(REVENUE_FORMULA, GROSS_AR_FORMULA);
  var DAYS_SALES_FORMULA = DIVIDE(MULTIPLY(GROSS_AR_FORMULA, CONSTANT(365)), REVENUE_FORMULA);
  var AR_FORMULA = TAG('1.1.100.100.100');
  var AR_ALLOWANCE_FORMULA = TAG('1.1.100.100.621');
  var UNBILLED_RECEIVABLE_FORMULA = TAG('1.1.100.120');
  var PROVISION_FOR_DOUBTFUL_ACCOUNTS_EXPENSE_FORMULA = TAG('2.6.505.145');
  var CUSTOMER_ADV_DEP_CUR_FORMULA = TAG('1.3.235');
  var CUSTOMER_ADV_DEP_NONCUR_FORMULA = TAG('1.4.235');
  var RETURNS_AND_ALLOWANCES_FORMULA = TAG('2.4.200');
  var REVENUE_RATIO_FORMULA = CONSTANT(1);
  var GROSS_MARGIN_FORMULA = DIVIDE(MINUS(REVENUE_FORMULA, TAG('2.5')), REVENUE_FORMULA);
  var NET_INCOME_FORMULA = DIVIDE(TAG('net_income'), REVENUE_FORMULA);
  /********* Formulas end ********/

  var rrrDefinition = new wpw.analysis.AnalysisDefinition();
  rrrDefinition.id = 'GJB_OJJXT5KqgPOdFE0O8Q';
  rrrDefinition.name = 'Revenue, Receivables and Receipts';

  // RRR - Mapped Items
  var GROSS_AR = new KPIModel('GROSS_AR', 'Account receivable', DATA_TYPE.MONETARY, true, '#E09951', GROSS_AR_FORMULA);
  var REVENUE = new KPIModel('REVENUE', 'Revenue', DATA_TYPE.MONETARY, true, wpw.analysis.KPIModel.BRIGHT_COLORS[0], REVENUE_FORMULA);
  var DEFER_REVENUE = new KPIModel('DEFER_REVENUE', 'Deferred revenue', DATA_TYPE.MONETARY, true,
      '#51E051', ADD(TAG('1.3.225'), TAG('1.4.225')));
  var CUSTOMER_ADV_DEP_CUR = new KPIModel('CUSTOMER_ADV_DEP_CUR', 'Customer advances and deposits, current',
      DATA_TYPE.MONETARY, true, null, CUSTOMER_ADV_DEP_CUR_FORMULA);
  var CUSTOMER_ADV_DEP_NONCUR = new KPIModel('CUSTOMER_ADV_DEP_NONCUR', 'Customer advances and deposits, noncurrent',
      DATA_TYPE.MONETARY, true, null, CUSTOMER_ADV_DEP_NONCUR_FORMULA);
  var componentModels = [REVENUE, GROSS_AR, DEFER_REVENUE, CUSTOMER_ADV_DEP_CUR, CUSTOMER_ADV_DEP_NONCUR];
  var component = new wpw.analysis.ComponentDefinition('Hk49KBhT', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('CUSTOMER_ADV_DEP', [CUSTOMER_ADV_DEP_CUR, CUSTOMER_ADV_DEP_NONCUR], false, 3);
  rrrDefinition.addUniqueKpiModels(componentModels);
  rrrDefinition.componentDefinitions.push(component);

  /// Accounts
  component = new wpw.analysis.ComponentDefinition('HyGnus0xfx', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = [AR_TAG_NUMBER];
  component.allowEdit = false;
  rrrDefinition.componentDefinitions.push(component);

  // RRR - Ratio Table
  var RECEIVABLE_PER = new KPIModel('RECEIVABLE_PERCENT', 'Receivables / Revenue', DATA_TYPE.PERCENTAGE,
    false, '#e4d354', RECEIVABLE_PER_FORMULA, 'Accounts receivable / Revenue');
  var AR_TURNOVER = new KPIModel('AR_TRUNOVER', 'AR turnover', DATA_TYPE.MONETARY, false, '#51E0E0',
    AR_TURNOVER_FORMULA, 'Credit sales / Accounts receivable', 'Measures the number of times trade receivables ' +
      'turnover during the year. The higher the turnover, the shorter the time between sale and cash collection.');
  var DAYS_SALES = new KPIModel('DAY_SALES', 'Days sales in receivables', DATA_TYPE.MONETARY, false,
    '#2b908f', DAYS_SALES_FORMULA, '(Accounts Receivable X 365) / Total sales', 'Measures the average length of time ' +
      'receivables are outstanding. Helps determine if potential collection problems exist.');
  rrrDefinition.componentDefinitions.push(new wpw.analysis.ComponentDefinition('ryxN9Yr2T', 'Ratios',
      COMPONENT_TYPES.TABLE, null, [RECEIVABLE_PER.key, AR_TURNOVER.key, DAYS_SALES.key]));
  rrrDefinition.kpiModels.push(RECEIVABLE_PER, AR_TURNOVER, DAYS_SALES);

  // RRR - Allowance
  var AR = new KPIModel('AR', 'Accounts receivable gross', DATA_TYPE.MONETARY, true, null, AR_FORMULA);
  var AR_ALLOWANCE = new KPIModel('AR_ALLOWANCE', 'Account receivable allowance', DATA_TYPE.MONETARY, true, null,
      AR_ALLOWANCE_FORMULA);
  var UNBILLED_RECEIVABLE = new KPIModel('UNBILLED_RECEIVABLE', 'Unbilled receivable', DATA_TYPE.MONETARY, true, null,
      UNBILLED_RECEIVABLE_FORMULA);
  var PROVISION_FOR_DOUBTFUL_ACCOUNTS_EXPENSE = new KPIModel('PROVISION_FOR_DOUBTFUL_ACCOUNTS_EXPENSE',
      'Provision for doubtful accounts expense', DATA_TYPE.MONETARY, true, null,
      PROVISION_FOR_DOUBTFUL_ACCOUNTS_EXPENSE_FORMULA);
  var RETURNS_AND_ALLOWANCES = new KPIModel('RETURNS_AND_ALLOWANCES', 'Returns and allowances', DATA_TYPE.MONETARY,
      true, null, RETURNS_AND_ALLOWANCES_FORMULA);
  var componentModels = [REVENUE, AR, AR_ALLOWANCE, UNBILLED_RECEIVABLE, PROVISION_FOR_DOUBTFUL_ACCOUNTS_EXPENSE,
    RETURNS_AND_ALLOWANCES];
  var component = new wpw.analysis.ComponentDefinition('BkWV9Kr3p', 'Allowance', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.drillDownKpiKeys = [REVENUE.key, GROSS_AR.key];
  rrrDefinition.componentDefinitions.push(component);
  rrrDefinition.addUniqueKpiModels(componentModels);

  // RRR - Revenue
  var REVENUE_RATIO = new KPIModel('REVENUE_RATIO', 'Revenue', DATA_TYPE.PERCENTAGE, true,
      wpw.analysis.KPIModel.BRIGHT_COLORS[0], REVENUE_RATIO_FORMULA, 'Revenue');
  var GROSS_MARGIN = new KPIModel('GROSS_MARGIN', 'Gross margin', DATA_TYPE.PERCENTAGE, false, null,
      GROSS_MARGIN_FORMULA, '(Revenue - Cost of sales) / Revenue');
  var NET_INCOME = new KPIModel('NET_INCOME', 'Net income', DATA_TYPE.PERCENTAGE, false, null, NET_INCOME_FORMULA,
    'Net income / Revenue');
  componentModels = [REVENUE_RATIO, GROSS_MARGIN, NET_INCOME];
  component = new wpw.analysis.ComponentDefinition('HkzN9trhT', 'Revenue', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.preferRight = true;
  rrrDefinition.componentDefinitions.push(component);
  rrrDefinition.addUniqueKpiModels(componentModels);

  wpw.injectedAnalysisDefinitions.push(rrrDefinition);
})(wpw);
