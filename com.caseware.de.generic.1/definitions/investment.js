(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var SHORT_TERM_INVESTMENTS_FORMULA = TAG('1.1.050');
  var LONG_TERM_INVESTMENTS_FORMULA = TAG('1.2.050');
  var AVAILABLE_FOR_SALE_CUR_FORMULA = TAG('1.1.050.170');
  var AVAILABLE_FOR_SALE_NON_CUR_FORMULA = TAG('1.2.050.170');
  var HELD_TO_MATURITY_CUR_FORMULA = TAG('1.1.050.180');
  var HELD_TO_MATURITY_NON_CUR_FORMULA = TAG('1.2.050.180');
  var TRADING_FORMULA = TAG('1.1.050.160');
  var OTHER_INVESTMENT_ST_FORMULA = TAG('1.1.050.190');
  var OTHER_INVESTMENT_LT_FORMULA = TAG('1.2.050.190');
  var GAIN_LOSS_INVESTMENT_FORMULA = TAG('2.7.100.110');
  var INVESTMENTS_UNREALIZED_FORMULA = TAG('2.7.100.120');
  var INVESTMENTS_REALIZED_FORMULA = TAG('2.7.100.125');
  var INTEREST_INCOME_FORMULA = TAG('2.7.100.500.100');
  var DIVIDEND_INCOME_FORMULA = TAG('2.7.100.500.200');
  var INVESTMENTS_IN_SUB_ASSOC_JV_FORMULA = TAG('1.2.155');
  var INVESTMENTS_IN_SUB_FORMULA = TAG('1.2.155.100');
  var INVESTMENTS_IN_ASSOC_FORMULA = TAG('1.2.155.110');
  var INVESTMENTS_IN_JV_FORMULA = TAG('1.2.155.120');
  var INCOME_FROM_SUB_ASSOC_JV_FORMULA = TAG('2.7.300');
  var TOTAL_ASSETS_FORMULA = ADD(TAG('1.1'), TAG('1.2'));
  var MARKET_SECURITIES_FORMULA = ADD(INVESTMENTS_REALIZED_FORMULA, INVESTMENTS_UNREALIZED_FORMULA);
  var INVESTMENTS_TO_TOTAL_ASSETS_FORMULA = DIVIDE(MARKET_SECURITIES_FORMULA, TOTAL_ASSETS_FORMULA);
  var INVESTMENT_IN_SAJV_TO_TOTAL_ASSETS_FORMULA = DIVIDE(TAG('1.2.155'), TOTAL_ASSETS_FORMULA);
  var INVESTMENT_INCOME_TO_INVESTMENTS_FORMULA = DIVIDE(TAG('2.7.100.500'), MARKET_SECURITIES_FORMULA);
  var INCOME_TO_INVESTMENTS_IN_SAJV_FORMULA = DIVIDE(INCOME_FROM_SUB_ASSOC_JV_FORMULA, TAG('1.2.155'));
  /********************* Formulas ends **********************/
  var investmentDef = new wpw.analysis.AnalysisDefinition();
  investmentDef.id = 'J2G7R956QzyKi8Mw58XZXw';
  investmentDef.name = 'Investments';

  // Overview
  var SHORT_TERM_INVESTMENTS = new KPIModel('SHORT_TERM_INVESTMENTS', 'Short-term investments', DATA_TYPE.MONETARY,
    true, null, SHORT_TERM_INVESTMENTS_FORMULA);
  var LONG_TERM_INVESTMENTS = new KPIModel('LONG_TERM_INVESTMENTS', 'Long-term investments', DATA_TYPE.MONETARY, true,
    null, LONG_TERM_INVESTMENTS_FORMULA);
  var INVESTMENTS_IN_SUB_ASSOC_JV = new KPIModel('INVESTMENTS_IN_SUB_ASSOC_JV',
    'Investments in subs, associates & jv\'s', DATA_TYPE.MONETARY, true, null, INVESTMENTS_IN_SUB_ASSOC_JV_FORMULA);
  var componentModels = [SHORT_TERM_INVESTMENTS, LONG_TERM_INVESTMENTS, INVESTMENTS_IN_SUB_ASSOC_JV];
  var component = new ComponentDefinition('S16VqYB3a', 'Overview', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN,
    wpw.analysis.modelsToKeys(componentModels));
  investmentDef.addUniqueKpiModels(componentModels);
  investmentDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('rJZ3_iAxzg', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['1.1.050', '1.2.050', '1.2.155'];
  component.allowEdit = false;
  investmentDef.componentDefinitions.push(component);

  // Ratios
  var INVESTMENTS_TO_TOTAL_ASSETS = new KPIModel('INVESTMENTS_TO_TOTAL_ASSETS',
    'Investments / Total assets', DATA_TYPE.PERCENTAGE, false, null,
    INVESTMENTS_TO_TOTAL_ASSETS_FORMULA);
  var INVESTMENT_IN_SAJV_TO_TOTAL_ASSETS = new KPIModel('INVESTMENT_IN_SAJV_TO_TOTAL_ASSETS',
    'Investments in subs, associates and jv’s / Total assets', DATA_TYPE.PERCENTAGE, false, null,
    INVESTMENT_IN_SAJV_TO_TOTAL_ASSETS_FORMULA);
  var INVESTMENT_INCOME_TO_INVESTMENTS = new KPIModel('INVESTMENT_INCOME_TO_INVESTMENTS',
    'Investment income / Investments', DATA_TYPE.PERCENTAGE, false, null,
    INVESTMENT_INCOME_TO_INVESTMENTS_FORMULA);
  var INCOME_TO_INVESTMENTS_IN_SAJV = new KPIModel('INCOME_TO_INVESTMENTS',
    'Income from subs, associates and jv’s / Investments in subs, associates and jv’s', DATA_TYPE.PERCENTAGE, false,
    null, INCOME_TO_INVESTMENTS_IN_SAJV_FORMULA);
  componentModels = [INVESTMENTS_TO_TOTAL_ASSETS, INVESTMENT_IN_SAJV_TO_TOTAL_ASSETS,
    INVESTMENT_INCOME_TO_INVESTMENTS, INCOME_TO_INVESTMENTS_IN_SAJV];
  component = new ComponentDefinition('ByRV9Yr3T', 'Ratios', COMPONENT_TYPES.TABLE, null,
    wpw.analysis.modelsToKeys(componentModels));
  investmentDef.kpiModels = investmentDef.kpiModels.concat(componentModels);
  investmentDef.componentDefinitions.push(component);

  // Marketable securities
  var TRADING = new KPIModel('TRADING', 'Trading securities', DATA_TYPE.MONETARY, true, null, TRADING_FORMULA);
  var AVAILABLE_FOR_SALE_CUR = new KPIModel('AVAILABLE_FOR_SALE_CUR', 'Available for sale securities, current',
      DATA_TYPE.MONETARY, true, null, AVAILABLE_FOR_SALE_CUR_FORMULA);
  var AVAILABLE_FOR_SALE_NON_CUR = new KPIModel('AVAILABLE_FOR_SALE_NON_CUR',
      'Available for sale securities, noncurrent', DATA_TYPE.MONETARY, true, null, AVAILABLE_FOR_SALE_NON_CUR_FORMULA);
  var HELD_TO_MATURITY_CUR = new KPIModel('HELD_TO_MATURITY_CUR', 'Held to maturity securities, current',
      DATA_TYPE.MONETARY, true, null, HELD_TO_MATURITY_CUR_FORMULA);
  var HELD_TO_MATURITY_NON_CUR = new KPIModel('HELD_TO_MATURITY_NON_CUR', 'Held to maturity, noncurrent',
      DATA_TYPE.MONETARY, true, null, HELD_TO_MATURITY_NON_CUR_FORMULA);
  var OTHER_INVESTMENT_ST = new KPIModel('OTHER_INVESTMENT_ST', 'Other short term investments', DATA_TYPE.MONETARY,
      true, null, OTHER_INVESTMENT_ST_FORMULA);
  var OTHER_INVESTMENT_LT = new KPIModel('OTHER_INVESTMENT_LT', 'Other long term investments', DATA_TYPE.MONETARY, true,
      null, OTHER_INVESTMENT_LT_FORMULA);
  var INVESTMENTS_UNREALIZED = new KPIModel('INVESTMENTS_UNREALIZED',
      'Investments, unrealized gain (loss)', DATA_TYPE.MONETARY, true, null, INVESTMENTS_UNREALIZED_FORMULA);
  var INVESTMENTS_REALIZED = new KPIModel('INVESTMENTS_REALIZED', 'Investments, realized gain (loss)',
      DATA_TYPE.MONETARY, true, null, INVESTMENTS_REALIZED_FORMULA);
  var INTEREST_INCOME = new KPIModel('INTEREST_INCOME', 'Interest income', DATA_TYPE.MONETARY,
      true, null, INTEREST_INCOME_FORMULA);
  var DIVIDEND_INCOME = new KPIModel('DIVIDEND_INCOME', 'Dividend income', DATA_TYPE.MONETARY,
      true, null, DIVIDEND_INCOME_FORMULA);
  componentModels = [TRADING, AVAILABLE_FOR_SALE_CUR, AVAILABLE_FOR_SALE_NON_CUR, HELD_TO_MATURITY_CUR,
    HELD_TO_MATURITY_NON_CUR, OTHER_INVESTMENT_ST, OTHER_INVESTMENT_LT, INVESTMENTS_UNREALIZED,
    INVESTMENTS_REALIZED, INTEREST_INCOME, DIVIDEND_INCOME];
  component = new ComponentDefinition('S11eNqFHna', 'Investments',
      COMPONENT_TYPES.CHART, CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.drillDownKpiKeys.push(SHORT_TERM_INVESTMENTS.key, LONG_TERM_INVESTMENTS.key);
  component.groupKeysInStack('AVAILABLE_FOR_SALE', [AVAILABLE_FOR_SALE_CUR, AVAILABLE_FOR_SALE_NON_CUR], false, 3);
  component.groupKeysInStack('HELD_TO_MANY', [HELD_TO_MATURITY_CUR, HELD_TO_MATURITY_NON_CUR], false, 4);
  component.groupKeysInStack('OTHER_INVESTMENT', [OTHER_INVESTMENT_ST, OTHER_INVESTMENT_LT], false, 5);
  component.groupKeysInStack('INVESTMENT_INCOME', [INTEREST_INCOME, DIVIDEND_INCOME], false, 6);
  investmentDef.kpiModels = investmentDef.kpiModels.concat(componentModels);
  investmentDef.componentDefinitions.push(component);

  // Investments in subs, associates & jv's
  var INVESTMENTS_IN_SUB = new KPIModel('INVESTMENTS_IN_SUB', 'Investments in subsidiaries', DATA_TYPE.MONETARY, true,
      null, INVESTMENTS_IN_SUB_FORMULA);
  var INVESTMENTS_IN_ASSOC = new KPIModel('INVESTMENTS_IN_ASSOC', 'Investments in associates', DATA_TYPE.MONETARY, true,
      null, INVESTMENTS_IN_ASSOC_FORMULA);
  var INVESTMENTS_IN_JV = new KPIModel('INVESTMENTS_IN_JV', 'Investments in joint ventures', DATA_TYPE.MONETARY, true,
      null, INVESTMENTS_IN_JV_FORMULA);
  var INCOME_FROM_SUB_ASSOC_JV = new KPIModel('INCOME_FROM_SUB_ASSOC_JV',
      'Income (loss) from subs, associates and jv’s', DATA_TYPE.MONETARY, true, null,
      INCOME_FROM_SUB_ASSOC_JV_FORMULA);
  var GAIN_LOSS_INVESTMENT = new KPIModel('GAIN_LOSS_INVESTMENT',
    'Gain (loss) on investments in subs, associates and jv’s', DATA_TYPE.MONETARY, true, null,
    GAIN_LOSS_INVESTMENT_FORMULA);
  componentModels = [INVESTMENTS_IN_SUB, INVESTMENTS_IN_ASSOC, INVESTMENTS_IN_JV, INCOME_FROM_SUB_ASSOC_JV,
    GAIN_LOSS_INVESTMENT];
  component = new ComponentDefinition('Hkxg45YH3p', 'Long Term Investments',
      COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.preferRight = true;
  component.drillDownKpiKeys.push(INVESTMENTS_IN_SUB_ASSOC_JV.key);
  component.groupKeysInStack('INVESTMENTS_IN', [INVESTMENTS_IN_SUB, INVESTMENTS_IN_ASSOC, INVESTMENTS_IN_JV], false, 2);
  investmentDef.addUniqueKpiModels(componentModels);
  investmentDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(investmentDef);
})(wpw);
