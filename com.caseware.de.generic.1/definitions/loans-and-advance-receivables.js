(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var CUR_GROUP_COMPANIES_FORMULA = TAG('1.1.108.100');
  var NONCUR_GROUP_COMPANIES_FORMULA = TAG('1.2.100.100');
  var CUR_SHAREHOLDERS_FORMULA = TAG('1.1.108.200');
  var NONCUR_SHAREHOLDERS_FORMULA = TAG('1.2.100.200');
  var CUR_OTHER_COMPANIES_FORMULA = TAG('1.1.108.300');
  var NONCUR_OTHER_COMPANIES_FORMULA = TAG('1.2.100.300');
  /********************* Formulas ends **********************/

  var loansAndAdvancesDef = new wpw.analysis.AnalysisDefinition();
  loansAndAdvancesDef.id = 'zIePYfZFTGGNOE4H3NhxPQ';
  loansAndAdvancesDef.name = 'Loans and advances receivable';

  // Overview
  var CUR_GROUP_COMPANIES = new KPIModel('CUR_GROUP_COMPANIES', 'Loans to group companies, current', DATA_TYPE.MONETARY,
      true, null, CUR_GROUP_COMPANIES_FORMULA);
  var NONCUR_GROUP_COMPANIES = new KPIModel('NONCUR_GROUP_COMPANIES', 'Loans to group companies, noncurrent',
      DATA_TYPE.MONETARY, true, null, NONCUR_GROUP_COMPANIES_FORMULA);
  var CUR_SHAREHOLDERS = new KPIModel('CUR_SHAREHOLDERS',
      'Loans to shareholders, board of director members, management, current', DATA_TYPE.MONETARY, true, null,
      CUR_SHAREHOLDERS_FORMULA);
  var NONCUR_SHAREHOLDERS = new KPIModel('NONCUR_SHAREHOLDERS',
      'Loans to shareholders, board of director members, management, noncurrent', DATA_TYPE.MONETARY, true, null,
      NONCUR_SHAREHOLDERS_FORMULA);
  var CUR_OTHER_COMPANIES = new KPIModel('CUR_OTHER_COMPANIES', 'Loans issued to other companies, current',
      DATA_TYPE.MONETARY, true, null, CUR_OTHER_COMPANIES_FORMULA);
  var NONCUR_OTHER_COMPANIES = new KPIModel('NONCUR_OTHER_COMPANIES', 'Loans issued to other companies, noncurrent',
      DATA_TYPE.MONETARY, true, null, NONCUR_OTHER_COMPANIES_FORMULA);
  var componentModels = [CUR_GROUP_COMPANIES, NONCUR_GROUP_COMPANIES,
                         CUR_SHAREHOLDERS, NONCUR_SHAREHOLDERS,
                         CUR_OTHER_COMPANIES, NONCUR_OTHER_COMPANIES];
  var component = new ComponentDefinition('BkHWiPfl0', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('GROUP_COMPANIES', [CUR_GROUP_COMPANIES, NONCUR_GROUP_COMPANIES], false, 1);
  component.groupKeysInStack('SHAREHOLDERS', [CUR_SHAREHOLDERS, NONCUR_SHAREHOLDERS], false, 2);
  component.groupKeysInStack('OTHER_COMPANIES', [CUR_OTHER_COMPANIES, NONCUR_OTHER_COMPANIES], false, 3);
  loansAndAdvancesDef.addUniqueKpiModels(componentModels);
  loansAndAdvancesDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('S1g8yeeWGx', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['1.1.108', '1.2.100'];
  component.allowEdit = false;
  loansAndAdvancesDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(loansAndAdvancesDef);
})(wpw);
