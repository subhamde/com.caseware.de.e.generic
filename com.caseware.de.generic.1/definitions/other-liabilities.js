(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var PENSION_AND_OTHER_RETIREMENT_BENEFIT_FORMULA = TAG('1.4.220');
  var CURRENT_OTHER_PROVISIONS_FORMULA = TAG('1.3.265');
  var NON_CURRENT_OTHER_PROVISIONS_FORMULA = TAG('1.4.265');
  var OTHER_PROVISIONS_FORMULA = ADD(CURRENT_OTHER_PROVISIONS_FORMULA, NON_CURRENT_OTHER_PROVISIONS_FORMULA);
  var CURRENT_OTHER_LIABILITIES_FORMULA = TAG('1.3.275');
  var NON_CURRENT_OTHER_LIABILITIES_FORMULA = TAG('1.4.275');
  var OTHER_LIABILITIES_FORMULA = ADD(CURRENT_OTHER_LIABILITIES_FORMULA, NON_CURRENT_OTHER_LIABILITIES_FORMULA);
  var ASSET_RETIREMENT_OBLIGATIONS_FORMULA = TAG('1.4.230');
  /********************* Formulas ends **********************/

  var otherLiabilitiesDef = new wpw.analysis.AnalysisDefinition();
  otherLiabilitiesDef.id = 'd7cYod0WRn68RsFMJprgZw';
  otherLiabilitiesDef.name = 'Other liabilities';

  // Overview
  var PENSION_AND_OTHER_RETIREMENT_BENEFIT = new KPIModel('PENSION_AND_OTHER_RETIREMENT_BENEFIT',
      'Pension and other retirement defined benefit', DATA_TYPE.MONETARY, true, null,
      PENSION_AND_OTHER_RETIREMENT_BENEFIT_FORMULA);
  var OTHER_PROVISIONS = new KPIModel('OTHER_PROVISIONS', 'Other provisions (Current and Noncurrent)',
      DATA_TYPE.MONETARY, true, null, OTHER_PROVISIONS_FORMULA);
  var OTHER_LIABILITIES = new KPIModel('OTHER_LIABILITIES', 'Other liabilities (Current and Noncurrent)',
      DATA_TYPE.MONETARY, true, null, OTHER_LIABILITIES_FORMULA);
  var ASSET_RETIREMENT_OBLIGATIONS = new KPIModel('ASSET_RETIREMENT_OBLIGATIONS', 'Asset retirement obligations',
      DATA_TYPE.MONETARY, true, null, ASSET_RETIREMENT_OBLIGATIONS_FORMULA);

  var componentModels = [PENSION_AND_OTHER_RETIREMENT_BENEFIT, OTHER_PROVISIONS,
                         OTHER_LIABILITIES, ASSET_RETIREMENT_OBLIGATIONS];
  var component = new ComponentDefinition('H1xbowGg0', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.LINE_COLUMN, wpw.analysis.modelsToKeys(componentModels));
  otherLiabilitiesDef.kpiModels = otherLiabilitiesDef.kpiModels.concat(componentModels);
  otherLiabilitiesDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('ryZnCofGfe', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['1.3.216', '1.3.265', '1.3.275', '1.3.330', '1.4.220', '1.4.225', '1.4.230',
    '1.4.265', '1.4.275'];
  otherLiabilitiesDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(otherLiabilitiesDef);
})(wpw);
