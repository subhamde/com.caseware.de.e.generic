(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var ADD = wpw.analysis.AnalysisFormula.FORMULAS.add;
  var DIVIDE = wpw.analysis.AnalysisFormula.FORMULAS.divide;
  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var GOODWILL_FORMULA = TAG('1.2.125.100');
  var GOODWILL_AA_IMP_FORMULA = TAG('1.2.125.200');
  var INTANGIBLE_FORMULA = TAG('1.2.130.100');
  var INTANGIBLE_AA_IMP_FORMULA = TAG('1.2.130.500');
  var AMORTIZATION_GOODWILL_FORMULA = TAG('2.6.500.102');
  var AMORTIZATION_OTHER_INT_FORMULA = ADD(TAG('2.5.100.107'), TAG('2.6.500.105'));
  var IMPAIRMENT_GOODWILL_FORMULA = TAG('2.6.500.120');
  var IMPAIRMENT_OTHER_INT_FORMULA = ADD(TAG('2.5.100.108'), TAG('2.6.500.115'));
  var GAIN_LOSS_FORMULA = TAG('2.7.100.105');
  var RESEARCH_DEVELOPMENT_FORMULA = TAG('2.6.500.000');
  var GROSS_GOODWILL_INTANGIBLE_FORMULA = ADD(GOODWILL_FORMULA, GOODWILL_AA_IMP_FORMULA, INTANGIBLE_FORMULA,
      INTANGIBLE_AA_IMP_FORMULA);
  var AMORTIZATION_GOODWILL_TO_GROSS_FORMULA = DIVIDE(AMORTIZATION_GOODWILL_FORMULA, GROSS_GOODWILL_INTANGIBLE_FORMULA);
  var AMORTIZATION_INT_GROSS_FORMULA = DIVIDE(AMORTIZATION_OTHER_INT_FORMULA, GROSS_GOODWILL_INTANGIBLE_FORMULA);
  var GOODWILL_AA_TO_GROSS_FORMULA = DIVIDE(GOODWILL_AA_IMP_FORMULA, GROSS_GOODWILL_INTANGIBLE_FORMULA);
  var INTANGIBLE_AA_TO_GROSS_FORMULA = DIVIDE(INTANGIBLE_AA_IMP_FORMULA, GROSS_GOODWILL_INTANGIBLE_FORMULA);
  /********************* Formulas end ***********************/

  var goodwillDef = new wpw.analysis.AnalysisDefinition();
  goodwillDef.id = 'g7gArgPLSpSwoXcYeHqBHQ';
  goodwillDef.name = 'Intangible assets and goodwill';

  // Overview
  var GOODWILL = new KPIModel('GOODWILL', 'Goodwill - Cost', DATA_TYPE.MONETARY, true, null, GOODWILL_FORMULA);
  var GOODWILL_AD_IMP = new KPIModel('GOODWILL_AD_IMP', 'Goodwill - Accumulated depreciation and impairment',
      DATA_TYPE.MONETARY, true, null, GOODWILL_AA_IMP_FORMULA);
  var INTANGIBLE_COST = new KPIModel('INTANGIBLE_COST', 'Intangible assets', DATA_TYPE.MONETARY, true, null,
    INTANGIBLE_FORMULA);
  var INTANGIBLE_AA_IMP = new KPIModel('INTANGIBLE_AA_IMP',
    'Intangible assets - Accumulated depreciation and impairment', DATA_TYPE.MONETARY, true, null,
    INTANGIBLE_AA_IMP_FORMULA);
  var AMORTIZATION_GOODWILL = new KPIModel('AMORTIZATION_GOODWILL', 'Amortization of goodwill', DATA_TYPE.MONETARY,
      true, null, AMORTIZATION_GOODWILL_FORMULA);
  var AMORTIZATION_OTHER_INT = new KPIModel('AMORTIZATION_OTHER_INT', 'Amortization of other intangible assets',
      DATA_TYPE.MONETARY, true, null, AMORTIZATION_OTHER_INT_FORMULA);
  var IMPAIRMENT_GOODWILL = new KPIModel('IMPAIRMENT_GOODWILL', 'Impairment of goodwill', DATA_TYPE.MONETARY, true,
      null, IMPAIRMENT_GOODWILL_FORMULA);
  var IMPAIRMENT_OTHER_INT = new KPIModel('IMPAIRMENT_OTHER_INT', 'Impairment of other intangible assets',
      DATA_TYPE.MONETARY, true, null, IMPAIRMENT_OTHER_INT_FORMULA);
  var GAIN_LOSS = new KPIModel('GAIN_LOSS', 'Gain (loss) on disposition of intangible assets', DATA_TYPE.MONETARY, true,
      null, GAIN_LOSS_FORMULA);
  var RESEARCH_DEVELOPMENT = new KPIModel('RESEARCH_DEVELOPMENT', 'Research and development', DATA_TYPE.MONETARY, true,
      null, RESEARCH_DEVELOPMENT_FORMULA);
  var componentModels = [GOODWILL, GOODWILL_AD_IMP, INTANGIBLE_COST, INTANGIBLE_AA_IMP,  AMORTIZATION_GOODWILL,
    AMORTIZATION_OTHER_INT, IMPAIRMENT_GOODWILL, IMPAIRMENT_OTHER_INT, GAIN_LOSS, RESEARCH_DEVELOPMENT];
  var component = new ComponentDefinition('S1mV9FHha', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  goodwillDef.kpiModels = goodwillDef.kpiModels.concat(componentModels);
  goodwillDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('SJXnCozfMl', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.allowEdit = false;
  component.breakdownGroupNumbers = ['1.2.125', '1.2.130'];
  goodwillDef.componentDefinitions.push(component);

  // Ratios
  var AMORTIZATION_GOODWILL_TO_GROSS = new KPIModel('AMORTIZATION_GOODWILL_TO_GROSS', 'Amortization expense - Goodwill',
      DATA_TYPE.PERCENTAGE, false, null, AMORTIZATION_GOODWILL_TO_GROSS_FORMULA);
  var AMORTIZATION_INT_TO_GROSS = new KPIModel('AMORTIZATION_INT_TO_GROSS', 'Amortization expense - Intangible assets',
      DATA_TYPE.PERCENTAGE, false, null, AMORTIZATION_INT_GROSS_FORMULA);
  var GOODWILL_AA_TO_GROSS = new KPIModel('GOODWILL_AA_TO_GROSS', 'Accumulated amortization - Goodwill',
      DATA_TYPE.PERCENTAGE, false, null, GOODWILL_AA_TO_GROSS_FORMULA);
  var INTANGIBLE_AA_TO_GROSS = new KPIModel('INTANGIBLE_AA_TO_GROSS', 'Accumulated amortization - Intangible assets',
      DATA_TYPE.PERCENTAGE, false, null, INTANGIBLE_AA_TO_GROSS_FORMULA);
  componentModels = [AMORTIZATION_GOODWILL_TO_GROSS, AMORTIZATION_INT_TO_GROSS, GOODWILL_AA_TO_GROSS,
    INTANGIBLE_AA_TO_GROSS];
  component = new ComponentDefinition('rkEVcYS3T', 'Ratios', COMPONENT_TYPES.TABLE, null,
      wpw.analysis.modelsToKeys(componentModels));
  goodwillDef.kpiModels = goodwillDef.kpiModels.concat(componentModels);
  goodwillDef.componentDefinitions.push(component);

  // Detailed - Cost
  goodwillDef.rootTagsToTraverse[INTANGIBLE_FORMULA.tagNumber] = INTANGIBLE_COST.key;
  var AMORTIZATION_OTHER_INT_COST = new KPIModel('AMORTIZATION_OTHER_INT_COST',
      'Amortization of other intangible assets', DATA_TYPE.MONETARY, true, null, TAG('2.5.100.107'));
  var IMPAIRMENT_OTHER_INT_COST = new KPIModel('IMPAIRMENT_OTHER_INT_COST',
      'Impairment of other intangible assets', DATA_TYPE.MONETARY, true, null, TAG('2.5.100.108'));
  component = new ComponentDefinition('rJ8V9FB3a', 'Intangible Assets and Goodwill',
     COMPONENT_TYPES.CHART,  CHART_TYPES.COLUMN, [AMORTIZATION_OTHER_INT_COST.key, IMPAIRMENT_OTHER_INT_COST.key,
        INTANGIBLE_COST.key], INTANGIBLE_COST.key, false, true);
  component.extraGroupsToLoad.push({key: INTANGIBLE_COST.key});
  goodwillDef.kpiModels.push(AMORTIZATION_OTHER_INT_COST, IMPAIRMENT_OTHER_INT_COST);
  goodwillDef.componentDefinitions.push(component);

  // Detailed - Accumulated amortization
  goodwillDef.rootTagsToTraverse[INTANGIBLE_AA_IMP_FORMULA.tagNumber] = INTANGIBLE_AA_IMP.key;
  var AMORTIZATION_OTHER_INT_AA = new KPIModel('AMORTIZATION_OTHER_INT_AA',
      'Amortization of other intangible assets - Accumulated amort and impairment', DATA_TYPE.MONETARY, true, null,
      TAG('2.6.500.105'));
  var IMPAIRMENT_OTHER_INT_AA = new KPIModel('IMPAIRMENT_OTHER_INT_AA',
      'Impairment of other intangible assets - Accumulated amort and impairment', DATA_TYPE.MONETARY, true, null,
      TAG('2.6.500.115'));
  component = new ComponentDefinition('H1HE5tB2T',
      'Intangible Assets and Goodwill - Accumulated depreciation and impairment', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, [AMORTIZATION_GOODWILL.key, AMORTIZATION_OTHER_INT_AA.key,
      IMPAIRMENT_GOODWILL.key, IMPAIRMENT_OTHER_INT_AA.key, INTANGIBLE_AA_IMP.key], INTANGIBLE_AA_IMP.key, false, true);
  component.extraGroupsToLoad.push({key: INTANGIBLE_AA_IMP.key});
  goodwillDef.kpiModels.push(AMORTIZATION_OTHER_INT_AA, IMPAIRMENT_OTHER_INT_AA);
  goodwillDef.componentDefinitions.push(component);

  goodwillDef.matchGroupSettings.push(new wpw.analysis.MatchGroupSetting([INTANGIBLE_FORMULA.tagNumber,
    INTANGIBLE_AA_IMP_FORMULA.tagNumber]));

  wpw.injectedAnalysisDefinitions.push(goodwillDef);
})(wpw);
