(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var PREPAID_EXP_CUR_FORMULA = TAG('1.1.140');
  var PREPAID_EXP_NONCUR_FORMULA = TAG('1.2.140');
  var DEF_COSTS_CUR_FORMULA = TAG('1.1.150');
  var DEF_COSTS_NONCUR_FORMULA = TAG('1.2.150');
  var OTHER_ASSETS_CUR_FORMULA = TAG('1.1.175');
  var OTHER_ASSETS_NONCUR_FORMULA = TAG('1.2.175');
  /********************* Formulas ends **********************/
  var prepaidOtherAssetsDef = new wpw.analysis.AnalysisDefinition();
  prepaidOtherAssetsDef.id = 'NgOcqeuZTPunH1Q2jKgYzg';
  prepaidOtherAssetsDef.name = 'Prepaid expenses and other assets';

  var PREPAID_EXP_CUR = new KPIModel('PREPAID_EXP_CUR', 'Prepaid expenses, current', DATA_TYPE.MONETARY, true, null,
      PREPAID_EXP_CUR_FORMULA);
  var PREPAID_EXP_NONCUR = new KPIModel('PREPAID_EXP_NONCUR', 'Prepaid expenses, noncurrent', DATA_TYPE.MONETARY, true,
      null, PREPAID_EXP_NONCUR_FORMULA);
  var DEF_COSTS_CUR = new KPIModel('DEF_COSTS_CUR', 'Deferred costs, current', DATA_TYPE.MONETARY, true, null,
      DEF_COSTS_CUR_FORMULA);
  var DEF_COSTS_NONCUR = new KPIModel('DEF_COSTS_NONCUR', 'Deferred costs, noncurrent', DATA_TYPE.MONETARY, true, null,
      DEF_COSTS_NONCUR_FORMULA);
  var OTHER_ASSETS_CUR = new KPIModel('OTHER_ASSETS_CUR', 'Other current assets', DATA_TYPE.MONETARY, true, null,
      OTHER_ASSETS_CUR_FORMULA);
  var OTHER_ASSETS_NONCUR = new KPIModel('OTHER_ASSETS_NONCUR', 'Other noncurrent assets', DATA_TYPE.MONETARY, true,
      null, OTHER_ASSETS_NONCUR_FORMULA);
  var componentModels = [PREPAID_EXP_CUR, PREPAID_EXP_NONCUR, DEF_COSTS_CUR, DEF_COSTS_NONCUR, OTHER_ASSETS_CUR,
    OTHER_ASSETS_NONCUR];
  var component = new ComponentDefinition('H1C6kiH2a', 'Overview', COMPONENT_TYPES.CHART,
      CHART_TYPES.COLUMN, wpw.analysis.modelsToKeys(componentModels));
  component.groupKeysInStack('PREPAID_EXP', [PREPAID_EXP_CUR, PREPAID_EXP_NONCUR], false, 1);
  component.groupKeysInStack('DEF_COSTS', [DEF_COSTS_CUR, DEF_COSTS_NONCUR], false, 2);
  component.groupKeysInStack('OTHER_ASSETS', [OTHER_ASSETS_CUR, OTHER_ASSETS_NONCUR], false, 3);
  prepaidOtherAssetsDef.addUniqueKpiModels(componentModels);
  prepaidOtherAssetsDef.componentDefinitions.push(component);

  // Accounts
  component = new ComponentDefinition('HJGUkxxWzl', 'Accounts', COMPONENT_TYPES.ACCOUNTS_TABLE);
  component.breakdownGroupNumbers = ['1.1.140', '1.1.150', '1.1.172', '1.1.175', '1.2.140', '1.2.150', '1.2.175'];
  component.allowEdit = false;
  prepaidOtherAssetsDef.componentDefinitions.push(component);

  wpw.injectedAnalysisDefinitions.push(prepaidOtherAssetsDef);
})(wpw);
