(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var incomeStatementDef = new wpw.analysis.AnalysisDefinition();
  incomeStatementDef.id = '8lG0qQx5TrqFOQAA1Ujg0Q';
  incomeStatementDef.name = 'Income statement';

  // Accounts
  var componentDef = new wpw.analysis.ComponentDefinition('HkgMlINffe', 'Accounts - Revenue', wpw.analysis.ComponentType.ACCOUNTS_TABLE);
  componentDef.allowEdit = false;
  componentDef.breakdownGroupNumbers = ['2.4'];
  incomeStatementDef.componentDefinitions.push(componentDef);

  componentDef = new wpw.analysis.ComponentDefinition('SklAVAHkXx', 'Accounts - Cost of Sales',
      wpw.analysis.ComponentType.ACCOUNTS_TABLE);
  componentDef.allowEdit = false;
  componentDef.breakdownGroupNumbers = ['2.5'];
  incomeStatementDef.componentDefinitions.push(componentDef);

  componentDef = new wpw.analysis.ComponentDefinition('rJZRNASJXx', 'Accounts - Operating Costs and Expenses',
      wpw.analysis.ComponentType.ACCOUNTS_TABLE);
  componentDef.allowEdit = false;
  componentDef.breakdownGroupNumbers = ['2.6.500'];
  incomeStatementDef.componentDefinitions.push(componentDef);

  componentDef = new wpw.analysis.ComponentDefinition('HyfR4Ar1me',
      'Accounts - Selling, General and Administrative Costs', wpw.analysis.ComponentType.ACCOUNTS_TABLE);
  componentDef.allowEdit = false;
  componentDef.breakdownGroupNumbers = ['2.6.505'];
  incomeStatementDef.componentDefinitions.push(componentDef);

  componentDef = new wpw.analysis.ComponentDefinition('Hy7040HJQe', 'Accounts - Other Income (Expenses)',
      wpw.analysis.ComponentType.ACCOUNTS_TABLE);
  componentDef.allowEdit = false;
  componentDef.breakdownGroupNumbers = ['2.7.100', '2.7.200', '2.7.300', '2.7.400', '2.7.500'];
  incomeStatementDef.componentDefinitions.push(componentDef);

  var importedComponentDefinitions = [
    'rybpyiS36', // Relationships
    'Hyvp1ir3p', // Revenue
    'BkOayjH26', // Cost of Sales
    'SyYa1irha' // Expenses
  ];
  angular.forEach(wpw.injectedAnalysisDefinitions, function(/** wpw.analysis.AnalysisDefinition */analysisDef) {
    for (var i = 0; i < analysisDef.componentDefinitions.length; i++) {
      var componentDef = analysisDef.componentDefinitions[i];
      if (importedComponentDefinitions.indexOf(componentDef.key) >= 0) {
        incomeStatementDef.componentDefinitions.push(componentDef);
        componentDef.initialKpiModelKeys.map(function(key) {
          var model = wpw.utilities.findObjectBy('key', analysisDef.kpiModels, key);
          if (model)
              incomeStatementDef.kpiModels.push(model);
        });
      }
    }
  });
  // Auto-load parent tags.
  incomeStatementDef.rootTagsToTraverse['2.4'] = 'TOTAL_REVENUE';
  incomeStatementDef.rootTagsToTraverse['2.5'] = 'TOTAL_COS';
  incomeStatementDef.rootTagsToTraverse['2.6'] = 'TOTAL_EXPENSE';

  wpw.injectedAnalysisDefinitions.push(incomeStatementDef);
})(wpw);

